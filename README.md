# scope-framework

This is the Scope framework applications should use to connect to a Scope device. It encapsulates the bluetooth stack and provide and easy to use interface to be able to implement and decouple the user experience from the various connection details.

## Documentation 

The documentation is generated using jazzy (https://github.com/realm/jazzy) and handled by the generate.sh script.
A pre-requisite to run the generate.sh script, jazzy needs to be installed via the command: 

```
[sudo] gem install jazzy
```

The documentation gets generated in the docs/swift_output.

## API Usage

The following code snippets show the API use for accessing Scope capabilities

### The AVScope class

The AVScope class is the main entry point to the Scope device. Such class can be used to access the device information as well as accessing underlying services to send data to the device or receiving data from the device.

All classes exposed to the AVScope class are singleton classes. Actually, AVScope itself is a singleton.

In order to get an AVScope instance, perform the following call:

```
let scope = AVScope.sharedInstance
```

In order to get the connected Scope device information, perform the following:

```
let deviceInfo = AVScope.sharedInstance.getConnectedDevice().getDeviceInfo()
```

It is also possible to scan for all available scope devices:

```
let devices = AVScope.sharedInstance.getAvailableScopeDevices() as? [ScopeDevice]
```

And finally connect to an instance of a scope Device:

```
try AVScope.sharedInstance.connectScopeDevice(scopeDevice)
```

#### The AVDeviceStatus class

As device status and probing are happening independently of the UI, the NSNotification iOS paradigm is used to inform any classes (observer) that register for such events. Two sets of methods to register and unregister to such events are provided by the AVScope class for device status and probing activities.

Register for device status notification from any class and trigger an handleDeviceStatus function would be done:

```
 AVScope.sharedInstance.registerForDeviceStatusNotification(self, selector: "handleDeviceStatus:")
```
The function that would post internally the "Processing" device status code is:

```
NSNotificationCenter.defaultCenter().postNotificationName(AVStatusDidChangeNotification, object: nil, userInfo: ["status": NSNumber(int: 2)])
```

The function to handle/process the device status would be defined as:

```
func handleDeviceStatus(notification: NSNotification) {
    if let info = notification.userInfo {
      let stat = info["status"] as? NSNumber
      let status = AVDeviceStatus(rawValue: stat!.integerValue )
      print("Device Status:, \(status)!")
    }
  }
```

Finally, in order to unregister the observer from such notifications, the following call would have to be issued:

```
AVScope.sharedInstance.unregisterFromDeviceStatusNotification(self)
```
In order to change the device state, for instance put the device in a ready state:

```
AVScope.sharedInstance.changeDeviceStatus(AVDeviceStatus.Ready);
```

### The slope service

The slope service allows to register for a constant stream of slope angle data.
The class that needs to register for slope data needs to implement the AVSlopeServiceDelegate

For example, if you would like to have the class: SlopeViewController listening for slope events, you would define it as:

```
class SlopeViewController: UIViewController, AVSlopeServiceDelegate {

 override func viewDidLoad() {
   super.viewDidLoad()
    AVScope.sharedInstance.getSlopeService().delegate = self;
    AVScope.sharedInstance.getSlopeService().startSlopeUpdates();
 }

 func didUpdateSlope(slopeAngle: Int) {
    print("Slope angle: \(slopeAngle)!")
 }

 func didUpdateSlopeWithError(error: NSError) {
    print("Error happened");
    // Let's stop the slope service
    AVScope.sharedInstance.getSlopeService().stopSlopeUpdates();
 }
}
```

Once the UI gathers the slope angle, the following method should be called: 
```
AVScope.sharedInstance.getSlopeService().stopSlopeUpdates();
```

For instance, it is a good idea to call this in the memory warning function provided by Cocoa on the specific view controller:

```
override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    AVScope.sharedInstance.getSlopeService().stopSlopeUpdates();
}
```

### The battery service

The battery service allows to register for a constant stream of battery level info.
The class that needs to register for slope data needs to implement the AVBatteryServiceDelegate

For example, if you would like to have the class: BatteryViewController listening for battery events, you would define it as:

```
class BatteryViewController: UIViewController, AVBatteryServiceDelegate {

 override func viewDidLoad() {
   super.viewDidLoad()
    AVScope.sharedInstance.getBatteryService().delegate = self;
    AVScope.sharedInstance.getBatteryService().startBatteryLevelUpdates();
 }

 func didUpdateBattery(batteryLevel: Int) {
    print("Battery Level: \(batteryLevel)!")
 }

 func didUpdateBatteryWithError(error: NSError) {
    print("Error happened");
    // Let's stop the battery service
     AVScope.sharedInstance.getBatteryService().stopBatteryLevelUpdates();
 }
}
```

The following method should be called to stop the battery level updates: 
```
AVScope.sharedInstance.getBatteryService().stopBatteryLevelUpdates();
```

For instance, it is a good idea to call this in the memory warning function provided by Cocoa on the specific view controller:

```
override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    AVScope.sharedInstance.getBatteryService().stopBatteryLevelUpdates();
}
```

### The snow profile service

The snow profile service allows to get available snow profiles as well as loading the profiles from Scope to the application.
The registration for new snow profiles is made via the AVScope class and uses a NSNotification. The userInfo object carries an array of snow profile identifiers and accessible via the "profiles" dictionary key (userInfo is a dictionary)

For example, if you would like to have the class: SnowProfileViewController listening for new snow profiles, you would define it as:

```
func handleSnowProfiles(notification: NSNotification) {
    if let info = notification.userInfo {
      let profiles = info["profiles"] as? [String]
      for profile in profiles {
         print("Profile Identifier: " + profile)
      }
    }
  }
```
Once the profile identifiers come back, the function: getSnowProfiles can be called, passing an array of desired snow profile identifiers in order to download data associates with such profiles. In the previous example, in the snowProfilesAvailable delegate function, we would insert the following line: 

```
 let snowProfiles = try AVScope.sharedInstance.getSnowProfileService().getSnowProfiles(profiles);
```

The snow profile service also allows for requesting the retrieval of such profiles at any time. The following code example illustrates such case:

```
do {
   let snowProfileIdentifiers = try AVScope.sharedInstance.getSnowProfileService().getAvailableSnowProfileIdentifiers();
   for profileIdentifier in snowProfileIdentifiers {
      print("Found new profile: \(profileIdentifier)")
   }
      
   let snowProfiles = try AVScope.sharedInstance.getSnowProfileService().getSnowProfiles(snowProfileIdentifiers);
   print("Downloaded \(snowProfiles.count) profiles");
} catch SnowProfileError.notFound(let profiles) {
   for profile in profiles {
     print("Could not find: profile \(profile)")
   }
} catch SnowProfileError.critical {
   print("Could not get the profiles at this moment")
} catch {
   // catches everything else
}
```

By default, only profile data is sent back to the application. The framework allows to also retrieve the raw snow profile data. There are 2 ways to do it, across the entire service by setting the rawDataEnabled property at the service level:

```
AVScope.sharedInstance.getSnowProfileService().enableRawData(true)
```

Or case by case when retrieving a set of snow profiles:

```
let snowProfiles = try AVScope.sharedInstance.getSnowProfileService().getSnowProfiles(true, profileIds:snowProfileIdentifiers);
```

