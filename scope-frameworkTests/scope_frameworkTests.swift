//
//  scope_frameworkTests.swift
//  scope-frameworkTests
//
//  Created by Olivier Brand on 2/25/16.
//  Copyright © 2016 Olivier Brand. All rights reserved.
//

import XCTest
import CoreBluetooth

@testable import scopeFramework

class scope_frameworkTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
  
    /**
     Tests AVProbing Error Code and AVDeviceStatus by calling an example of each that should return a real value and one that should not (maybe should loop through all/ test a random one?)
     */
    func testEnum() {
        // Test AVProbing Error Code
        XCTAssert(AVProbingErrorCode(status:6) == AVProbingErrorCode.Icing, "error code does not match")
        XCTAssert(AVProbingErrorCode(status:12) == nil, "invalid number did not return nil")
        
        // Test AVDeviceStatus 
        XCTAssert(AVDeviceStatus(status:5) == AVDeviceStatus.Slope, "error code does not match")
        XCTAssert(AVDeviceStatus(status:9) == nil, "invalid number did not return nil")
     
    }
    
    
    //MARK: AVScopeTests
    
    func testGetAvailibleScopeDevices() {
        // BLE Stuff.. 
    }
    
    func testConnectScopeDevice() {
        //BLE Stuff ...
    }
    
    /**
     Calls getConnected Device, skips BLEStep and calls newDeviceInfo.  Assumes BLE sends data for all characteristics
     */
    func testGetConnectedDevice() {
        
        // placeholder deviceInfo
        var newDevice = AVScopeDevice(deviceInfo: AVDeviceInfo(hardwareRevision: " ", serialNumber: " ", modelNumber: " ", manufacturerName: " ", firmwareRevision: " ", systemID: AVSystemId(manufacturerId: 0, organizationallyUniqueId: 0)))
        
        // Call get Connected Device
        AVScope.sharedInstance.getConnectedDevice({(scopeDevice) -> () in
            newDevice = scopeDevice
        print(newDevice)
        })
        
        //createFakeData
        let fakeDeviceInfo = AVDeviceInfo(hardwareRevision: "2.0.3", serialNumber: "34526AB", modelNumber: "ScopeV3", manufacturerName: "Avatech", firmwareRevision: "3.1.1", systemID: AVSystemId(manufacturerId: 655895223496, organizationallyUniqueId: 11379911))
        let fakeDevice = AVScopeDevice(deviceInfo: fakeDeviceInfo)
        
        
        let fakeStringData = ["Hardware Revision":fakeDeviceInfo.hardwareRevision,"Serial Number":fakeDeviceInfo.serialNumber,"Model Number": fakeDeviceInfo.modelNumber ,"Manufacturer Name":fakeDeviceInfo.manufacturerName,"Firmware Version": fakeDeviceInfo.firmwareRevision]
        
        var fakeData = [String:NSData]()
        for (characteristic, stringIn) in fakeStringData {
            let stringInNS = stringIn as NSString
            let data = NSData(bytes: stringInNS.UTF8String, length: stringInNS.length)
            fakeData[characteristic] = data
        }
        
        var fakeSystemID: UInt64 = 12512345123451231432
        let data = NSData(bytes: &fakeSystemID, length: sizeof(Int))
        
        
        // Call new Device Info (BLE Method) for each characteristic
        for (characteristic, data) in fakeData {
        AVScope.sharedInstance.newDeviceInfo(data, characteristic: characteristic)
        }
        AVScope.sharedInstance.newDeviceInfo(data, characteristic: "System ID")
        
        
       // Assertions 
        XCTAssert(fakeDevice.getDeviceInfo().hardwareRevision == newDevice.getDeviceInfo().hardwareRevision, "hardware revision does not match")
        XCTAssert(fakeDevice.getDeviceInfo().serialNumber == newDevice.getDeviceInfo().serialNumber, "Serial Number does not match")
        XCTAssert(fakeDevice.getDeviceInfo().modelNumber == newDevice.getDeviceInfo().modelNumber, "Model Number does not match")
        XCTAssert(fakeDevice.getDeviceInfo().manufacturerName == newDevice.getDeviceInfo().manufacturerName, "Manufacturer Name does not match")
        XCTAssert(fakeDevice.getDeviceInfo().firmwareRevision == newDevice.getDeviceInfo().firmwareRevision, "Firmware Revision does not match")
        XCTAssert(fakeDevice.getDeviceInfo().systemID.manufacturerId == newDevice.getDeviceInfo().systemID.manufacturerId, "manufacturer ID does not match")
        XCTAssert(fakeDevice.getDeviceInfo().systemID.organizationallyUniqueId == newDevice.getDeviceInfo().systemID.organizationallyUniqueId, "Org ID does not match")
        
    }
    
    
     //MARK: AVBatteryServiceTests
    
    /**
     Tests Battery Service.  Calls "getBatteryLevel", skips BLE step and artificially calls "battery updated" with fake battery data.
     */
    func testBatteryService() {
        
        var newlevel = 0
        
        // get battery level, includes completion handler
        AVBatteryService.sharedInstance.getBatteryLevel({(batteryLevel) -> () in
            newlevel = batteryLevel
        })
        
        // create fake battery level data
        var fakeBatteryLevel: Int = 57
        let data = NSData(bytes: &fakeBatteryLevel, length: sizeof(Int))
        
        // battery updated - will normally be called from BLE (this assumes the BLEMAnager is functioning)
        AVBatteryService.sharedInstance.batteryUpdated(data, error: nil)
        
        // check to see if battery level passed to completion handler = fake battery level
        XCTAssert(newlevel == fakeBatteryLevel)
        
    }
    
    
    
    
    //MARK: AVSlopeServieTests
    
    /**
     Tests AVSlopeService.  Tests delegate and slope updated function. DOES NOT test BLE or start/stop slope updates methods.
     */
    /*
    /// Fake class to mock slope delegate
    class fakeSlopeDelegate: AVSlopeServiceDelegate {
        var newSlope = 0
        @objc func didUpdateSlope(slopeAngle: Int) {
            newSlope = slopeAngle
        }
    }
    
    /**
    Calls slopeUpdated and checks to see if fake delegate recieves updated slope.
     */
    func testSlopeService() {
        
        let fakeUI = fakeSlopeDelegate()
        
        // fake slope data
        var fakeSlopeAngle = 78
        let data = NSData(bytes: &fakeSlopeAngle, length: sizeof(Int))

        AVSlopeService.sharedInstance.delegate = fakeUI
        AVSlopeService.sharedInstance.slopeUpdated(data, error: nil)
        
        
        XCTAssert(fakeSlopeAngle == fakeUI.newSlope)
        
    }
    */
    //MARK: AVSnowProfilesTests
    
     /**
     Calls getAvailibleSnowProfileIdentifiers.  Fakes the BLE stuff and calls new Profile IDs (tests completion method and data conversion)
     */
    func testGetSnowProfilesID() {
        var profileIds = ["empty"]
        AVSnowProfileService.sharedInstance.getAvailableSnowProfileIdentifiers({(ids)  -> () in
        profileIds = ids
        })
        
        // Create Fake Ids (using code from writeProfileIDs)
        let fakeIds = ["A123","A235"]
        let fullString:String = fakeIds.joinWithSeparator(",")
        let fullNSString = fullString as NSString
        let data = NSData(bytes: fullNSString.UTF8String, length: fullNSString.length)
        
        // Call newProfileIds - skips BLE stuf
        AVSnowProfileService.sharedInstance.newProfileIds(data, error: nil)
        
        // Check if fake Ids match the profiles recieved back
        XCTAssert(profileIds == fakeIds,"ID arrays do not match")
    }
    
    
    func testGetSnowProfiles() {
        
    }
    func testGetRawSnowProfiles() {
        
    }
    func testDeleteSnowProfiles() {
        
    }
    
    /**
     Tests add, delete, and edit methods of the profileAnnotation test 
     */
    func testAnnotations() {
        let aProfileAnnotation = ProfileAnnotations()
        aProfileAnnotation.addComment(34, comment: "comment 34", importantFlag: true)
        aProfileAnnotation.addComment(59, comment: "comment 59", importantFlag: false)
        aProfileAnnotation.addComment(89, comment: "comment 89", importantFlag: true)
        
        XCTAssert(aProfileAnnotation.comments.count == 3)
        
        aProfileAnnotation.deleteComment(10)
        
        XCTAssert(aProfileAnnotation.comments.count == 2)
        
        aProfileAnnotation.editComment(90, newDepth: 14, newtext: "comment 14", newFlag: false)
        
        XCTAssert(aProfileAnnotation.comments.count == 2)
        
        let test1 = aProfileAnnotation.comments[0]
        let test2 = aProfileAnnotation.comments[1]
        
        print(test1)
        print(test2)
        if test1.depth == 14 {
            XCTAssert(test1.text == "comment 14")
        }
        else {
           XCTAssert(test2.text == "comment 14")
        }
        
    }
    
    /**
     <#Description#>
     */
  
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
      
            
        }
    }
    
}
