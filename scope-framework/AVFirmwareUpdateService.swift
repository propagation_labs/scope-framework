//
//  AVFirmwareUpdateService.swift
//  scope-framework
//
//  Created by Olivier Brand on 3/1/16.
//  Copyright © 2016 Olivier Brand. All rights reserved.
//

import UIKit
import iOSDFULibrary
import CoreBluetooth

/// The AVFirmwareUpdateServiceDelegate protocol defines the methods used to trigger
/// a firmware update from a AVFirmwareUpdateService object.
/// Upon receiving a firmware update progress, you can use the result to update
/// your user interface or perform other actions. If the firmware could not be performed 
/// or failed,a specific error message is sent back
/// you might want to stop updates for a short period of time and try again later.
/// You can use the stopFirmwareUpdate of AVFirmwareUpdateService to stop the firmware update.


@objc
public protocol AVFirmwareUpdateServiceDelegate: class {
  /**
   Called when the firmware is updating so the client knows the overall progress
   
   - parameter progress: the progress in % completion
   */
  @objc optional func firmwareUpdatingWithProgress(_ progress: Int)
    
    
    /**
     Called when the status of the firmware update changes 
 */
    @objc optional func firmwareUpdatingChangedStatus(_ status: String)
  
  /**
   Called when the firmware has succesfully finished updating
   */
   func firmwareUpdatingCompleted()
  
  /**
   Called when an error occurred
   
   - parameter error: error message
   */
   func firmwareUpdatingFailed(_ error: String)
    
  
}


// Enum to contain instructions for the "start charachteristic" for firmware update service.  These can be read or written over the firmware command characteristic
public enum firmwareInstruction : Int {

    case startArm = 1 // start the arm update
    case startPic = 2 // start the pic update
    case ACK = 3 // pic packet sent successfully
    case NACK = 4 // pick packet failed
    case donePIC = 5 // pic is done with firmware update
    case restart = 6 // to only be called after Pic update (if arm update occurs, it will restart both automatically)
}




/// The firmware update service defines the necessary functions to manage the firmware update
/// on a Scope device
// IN the short term, if firmware needs to be updated:
// 1. add the firmware file(s) to the project 
// 2. update the firmwareVersionDict 
// 3. update the pic and/or armFirmwareFile Dicts


// The basic function of the firmware update serice is as follows. 
// 1. startFirmwareUpdate starts the process
// 2. startFirmwareUpdate uses the functions in the "Firmware Version" section to determine if the pic, arm, or both need to be updated 
// 3. either the Pic or Arm update will be started, depending on which is needed (if both are, pic will go first)
// 
// FOR PIC UPDATE
// 1. Parses Pic firmware file into 12 line data objects to send
// 2. sends a startPic command to the Scope
// 3. transfers one package of 12 lines at a time, waiting for an ACK back from the Scope between each packet that is transferred
// 4. receives a complete notification from Scope when it is done, and either starts Arm update (if necessary) or sends restart command to Pic 
// 
// FOR ARM UPDATE 
// 1.  send the startArm command to Scope, also set DFU disconnect to true
// 2. Scope will disconnect, and change to DFU mode
// 3. Scope will search for the DFU device, and autoconnect when it is discovered
// 4. StartArmDFUProcess is called.  This loads the arm firmware and starts transfer
// 5. Scope will update the transfer status through the delegate methods
// 6. upon completion, the delegate will call the didStateChangedTo: .completed method, and the completion delegate method will be called to the UI.  Firmware update is complete.


open class AVFirmwareUpdateService: NSObject,DFUServiceDelegate,DFUProgressDelegate,LoggerDelegate {

  //Singleton
  open static let sharedInstance = AVFirmwareUpdateService()
  
  //The delegate used to trigger the FirmwareUpdateServiceDelegate functions
  open weak var delegate:AVFirmwareUpdateServiceDelegate?
    
  // connection completion handler
    open var connectedDFUCompletionMethod: (Void)-> Void = {x in x}

    
    // ARM variables
    var armNeedsUpdating = false
    private var armDfuController    : DFUServiceController?
    private var armProgress = 0 // progress in 0-100
    private var expectedArmTime : Float  = 20 // in sec, should measure
    private var armURL : URL? // url for latest Arm firmware version
    
    //PIC variables 
    var picNeedsUpdating = false
    private var picProgress = 0  // progress in 0-100
    private var picFirmware : [Data]? // array of pic firmware data objects broken up by size to be sent at once
    var picPacketsTransferred = 0 // counts the number of pic data packets that have been transferred (objects in picFirmware Array)
    private var expectedPicTime  : Float = 45 // in sec, should measure
    var cancelFirmwareUpdateTimer = Timer()  // timer to exit from firmware update if it is exceeded
    var NackCounter = 0 //counter to count number of NACKS received in a row (used to cancel firmware update if it is too many)

    // other variables
    private var updateStatus = "String" 
    var updateType = "Latest" // can be Latest, Calibration, or Dev
    
    
    //dev variables 
    let devPicFilePath = "/Development/PicDev.hex"
    let devArmFilePath = "/Development/ArmDev.zip"
    //cal variables
    let calArmFilePath = "/Calibration/ArmCal.zip"
    //MARK: - Public Functions
    
  /**
   Starts the firmware update.  Checks what needs to be updated, and then starts either the pic or the arm depending on what should be updated
     
   */
    open func startFirmwareUpdate() {
        // reset progress
        picProgress = 0
        armProgress = 0
        
        
        switch updateType {
        case "Calibration":
            startArmUpdate(withFilePath: calArmFilePath)
        case "Development":
            picNeedsUpdating = true
            armNeedsUpdating = true
            
            startPicUpdate(withFilePath: devPicFilePath)
            
            
        case "Latest":
            
            if let currentFirmwareString = AVScope.sharedInstance.getBLEManager().activeScopeDevice?.info.firmwareRevision {
                
                // check what needs to be updating
                (picNeedsUpdating, armNeedsUpdating) = checkWhatNeedsUpdating(currentFirmware: currentFirmwareString)
                
                print("arm update =\(armNeedsUpdating), pic update =\(picNeedsUpdating)")
                
     
                // set expected time for UI depending on what needs updating
                if !picNeedsUpdating {
                    expectedPicTime = 0
                }
                if !armNeedsUpdating {
                    expectedArmTime = 0
                }
                
                // check which processor needs update (if both, start with pic)
                if picNeedsUpdating {
                    startPicUpdate(withFilePath: DropBoxFirmwareManager.sharedInstance.picFilePath!)
                }
                else if armNeedsUpdating {
                    startArmUpdate(withFilePath: DropBoxFirmwareManager.sharedInstance.armFilePath!)
                }
                else {
                    print("neither pic nor arm needs an update")
                }
            }

        default:
            print("invalid type to update to")
        }
        
        
        
  }
  
  /**
   Stops the firmware update
   */
  open func stopFirmwareUpdate() {
    
    // check if arm is updating
    if armDfuController != nil {
        armDfuController?.abort()
    }

  }
    
    /* Not necessary
  
  /**
   Check if a firmware update is available
   
   - parameter version: The lastest version known by the mobile app
   
   - returns: YES if an update should be performed, NO otherwise
   */
  open func firmwareAvailable(_ version: String) -> Bool {
    
    // Send back the comparison of the 2 revisions
    //return AVScope.sharedInstance.getConnectedDevice().getDeviceInfo().firmwareRevision == version
    return true
  }
  */
  
    
    
    /**
     Combines progress from both Pic and Arm and outputs a combined progress level via the delegate method (Int 0-100)
 */
    func updateCombinedProgress() {
       
        let expectedtotalTime = expectedArmTime + expectedPicTime
        
        //time completed from each firmware update
        let armTime = Float(armProgress) * expectedArmTime/100
        let picTime = Float(picProgress) * expectedPicTime/100
        
        let percentComplete = (armTime + picTime)/expectedtotalTime * 100
        
        // update delegate
        delegate?.firmwareUpdatingWithProgress!(Int(percentComplete))
    }
    
    
    //MARK: - ARM Functions
    
    /**
     Called to initiate Arm update. updates UI, tells Scope to switch to DFU mode.  This will cause a disconnect
 */
    func startArmUpdate(withFilePath: String ) {
        self.delegate?.firmwareUpdatingChangedStatus!("Downloading ARM Firmware")
        
        //downloadArmFile
        DropBoxFirmwareManager.sharedInstance.downloadFirmwareFile(withPath: withFilePath, completionHandler: {URL in
            self.delegate?.firmwareUpdatingChangedStatus!("Starting ARM update")
            self.armURL = URL
            AVScope.sharedInstance.getBLEManager().DFUDisconnect = true
            AVScope.sharedInstance.getBLEManager().writeFirmwareInstruction(.startArm)
            
        })
        
    }
    
    /**
     Connects to device in Arm DFU update mode
     */
    open func connectDFUDevice(_ DFUPeripheral: CBPeripheral, completionHandler: ((Void) -> Void)?) {
        
        connectedDFUCompletionMethod = completionHandler!
        AVScope.sharedInstance.getBLEManager().connect(DFUPeripheral)
    }
    
    
    
    /**
     Starts DFU Process for ARM (is called when it reconnects in DFU mode)
     */
    func startARMDFUProcess() {
        
        guard AVScope.sharedInstance.getBLEManager().isConnected  else {
            print("No Scope Connected, cannot update firmware")
            return
        }
        // gets firmware
        let selectedArmFirmware = DFUFirmware(urlToZipFile: armURL!)!
        
        // sets up initiator
        let centralManager = AVScope.sharedInstance.getBLEManager().centralManager
        let dfuPeripheral = AVScope.sharedInstance.getBLEManager().activePeripheral
        let dfuInitiator = DFUServiceInitiator(centralManager: centralManager!, target: dfuPeripheral!).withFirmwareFile(selectedArmFirmware)
        
        // set delegates
        dfuInitiator.delegate = self
        dfuInitiator.progressDelegate = self
        dfuInitiator.logger = self
        
        // start dFU (and set controller)
        armDfuController = dfuInitiator.start()
      
    }
    
    
    
    //MARK: DFUServiceDelegate
    
    /**
     called when state of DFU on ARM changes.  Updates UI with delegate method, and calls finishing delegate methods if it is completed
 */
    open func didStateChangedTo(_ state:DFUState) {
        // Update this section with notifications or logic based on case
        let armDFUStatus = state.description()
        logWith(LogLevel.info, message: "Arm DFU Changed state to: \(armDFUStatus)")
        
        //update delegate with status 
        let fullStatus = "Arm status = " + armDFUStatus
        delegate?.firmwareUpdatingChangedStatus!(fullStatus)
        
        
        if state == .completed {
            
            //set arm update to finished
            armNeedsUpdating = false  // need to make sure this flag is set before disconnect occurs! (otherwise it will scan for all devices and not just scope!
            
            
            //finished (arm is always 2nd)
            print("firmware updated completed")
            delegate?.firmwareUpdatingCompleted()
            
            // after this, scope will auto restart into normal mode
        }
    }
    
    /**
     Called when an error occurs during ARM DFU update, updates UI through delegate failed message
 */
    open func didErrorOccur(_ error: DFUError, withMessage message: String) {
        //log locally
        logWith(LogLevel.error, message: message)
        // call error over delegate
        delegate?.firmwareUpdatingFailed(message)
       
    }
    
    
    //MARK:  DFUProgressDelegate
    /** 
     updates the progress of the ARM DFU transfer
 */
    open func onUploadProgress(_ part: Int, totalParts: Int, progress: Int, currentSpeedBytesPerSecond: Double, avgSpeedBytesPerSecond: Double) {
        armProgress = progress
        updateCombinedProgress()
        // could add functionality to display speed
    }
    
    //MARK: LoggerDelegate
    // logging function for ARM DFU
    open func logWith(_ level:LogLevel, message:String){
        print("\(level.name()) : \(message)")
    }

    
    
    
    
    
    //MARK: - Pic Functions
    
    /**
     begins updated Pic firmware transfer by converting pic firmware file and calling .startPic command over BLE
 */
    func startPicUpdate(withFilePath: String ) {
        
        delegate?.firmwareUpdatingChangedStatus!("Downloading Pic Firmware")
        //download pic firmware.  Put the rest of this in completion method of download
        
        print("pic file path = \(DropBoxFirmwareManager.sharedInstance.picFilePath!)")
    
        DropBoxFirmwareManager.sharedInstance.downloadFirmwareFile(withPath: withFilePath, completionHandler: {URL in
            // get picFirmwareData from URL, convert it to digestable chunks, then send start command to Scope.  Waits for notification before transferring the rest of the data
            self.delegate?.firmwareUpdatingChangedStatus!("Starting Pic Update")
            self.picFirmware = self.makePicFirmwareArray12(fromURL: URL)
            AVScope.sharedInstance.getBLEManager().writeFirmwareInstruction(.startPic)
        
        })
        
      
        
    }
    /*
    /**
     Converts Pic Firmware File into array of data objects separated by line in file. NOT USED ANYMORE
 */
    func makePicFirmwareArray() -> [Data]? {
        do {
            let fullPicFirmwareString = try String(contentsOf: getLatestPicFirmwareURL() , encoding: .utf8)
           let StringArray = fullPicFirmwareString.components(separatedBy: .newlines)
           var dataArray = [Data]()
            for line in StringArray {
                let lineData = line.dataFromHexadecimalString()
                dataArray.append(lineData!)
            }
            
            return dataArray
        }
        catch {
             print("could not create data from contents of pic URL")
    }
        return nil
    }
    
 */
    
    /**
     Converts Pic Firmware File into array of data objects separated into chunks of 12 lines.  Also adds length of packet to beginning.  Data is hex format
     */
    func makePicFirmwareArray12(fromURL: URL) -> [Data]? {
        do {
            let fullPicFirmwareString = try String(contentsOf: fromURL , encoding: .utf8)
            let colonlessStringArray = fullPicFirmwareString.components(separatedBy: ":")

            var StringArray = [String]()
            for colonlessString in colonlessStringArray {
                if !colonlessString.isEmpty {
                let trimmedString = colonlessString.trimmingCharacters(in: .whitespacesAndNewlines)
                //print("strLen = \(colonlessString.characters.count), trimmedL = \(trimmedString.characters.count)")
                //print("\(trimmedString)")
                StringArray.append(":" + trimmedString)
                }
               // print("line")
            }
            
            
            print("StringArrayCount = \(StringArray.count)")
            
            // combine string arrays into sections of 12 
            let numToCombine = 12
            var counter = 0
            var combinedStrArray = [String]()
            while counter < (StringArray.count) {
                var largeString = String()
                var endofRange  = counter + numToCombine
                // check if there are enough packets to combine 12
                if endofRange >= StringArray.count {
                    // at end, just go to last packet
                    endofRange = StringArray.count
                }
                
                for i in counter..<endofRange{
                    largeString += StringArray[i]
                }
                combinedStrArray.append(largeString)
                counter += numToCombine
            }
            
            
            
            var dataArray = [Data]()
            for section in combinedStrArray {
                           //convert to hex
                let combinedLinesData = section.dataFromHexadecimalString()
                // add 1 byte of length to beginning
                var newDataLength = UInt8(combinedLinesData!.count + 1)
                let finalCombinedLineData = NSMutableData(bytes: &newDataLength, length: 1)
                finalCombinedLineData.append(combinedLinesData!)
                
                dataArray.append(finalCombinedLineData as Data)
            }
            print(dataArray)
            print("num packets = \(dataArray.count )")
            return dataArray
        }
        catch {
            print("could not create data from contents of pic URL")
        }
        return nil
    }
    
    
    /**
     Called when new Data is received over the Pic Update characteristic.  Checks to see what command it is, and acts accordingly
     */
    func newPicFirmwareData (data: Data) {
        let receivedNumber = data.convertFromInt8(0, range: 1)[0]
        
     
        switch receivedNumber {
            
        // ready for next packet, send next if there are more to transfer
        case firmwareInstruction.ACK.rawValue :
            
            // reset nack counter
            NackCounter = 0
            
            // update progress
            picProgress = (picPacketsTransferred*100)/picFirmware!.count
            updateCombinedProgress()
            delegate?.firmwareUpdatingChangedStatus!("transferring Pic Packet \(picPacketsTransferred + 1) of \(picFirmware!.count)")
            
            
            // still more packets to transfer, send next one
            if picPacketsTransferred < picFirmware!.count {
                transferPicFirmware(packetNumberToTransfer: picPacketsTransferred)
                // update number of packets transferred
                picPacketsTransferred += 1
            }
            
            // all packets have been transferred
            else {
                delegate?.firmwareUpdatingChangedStatus!("Completed Pic Transfer")
                print("all pic firmware packets transferred")
            }
        
        
        // completed updating pic
        case firmwareInstruction.donePIC.rawValue :
            print("done pic")
            completedPicUpdate()
            
        // false acknowledge, try to resend previous packet
        case firmwareInstruction.NACK.rawValue :
            print("NACK!")
            NackCounter += 1
            if NackCounter > 4 { // there is a serious issue, attempt to restart pic transfer
                switch updateType {
                case "Development":
                startPicUpdate(withFilePath: devPicFilePath)
                default:
                startPicUpdate(withFilePath: DropBoxFirmwareManager.sharedInstance.picFilePath!)

                }
            }
            else { // try to resend previous packet
            transferPicFirmware(packetNumberToTransfer: picPacketsTransferred-1)
            }
        default:
            break
        
        }
    }
    
    /**
     sends packet of Pic Firmware to Scope
     
     packetNumberToTransfer - index of packet number to transfer in PicFirmware Array
 */
    func transferPicFirmware(packetNumberToTransfer: Int) {
    
        let packetToTransfer = picFirmware![packetNumberToTransfer]
        //print("packet to trans #bytes = \(packetToTransfer.count)")
        print("trans packet \(picPacketsTransferred)")
        print("packetToTransfer = \(packetToTransfer.hexEncodedString() )")
        AVScope.sharedInstance.getBLEManager().writeRawData( packetToTransfer, characteristic: "Firmware Data")
    }
    
    
 
    
    /**
     Called when the Pic completes its update.  Update the UI and check if other updates need to happen
 */
    func completedPicUpdate() {
        
        // update UI
        picProgress = 100
        updateCombinedProgress()
        delegate?.firmwareUpdatingChangedStatus!("Completed Pic Update")
        
        // reset buffers
        picNeedsUpdating = false
        picPacketsTransferred = 0
       
        //Either start the arm update, or call the restart command
        
        if armNeedsUpdating {
            // start arm update process
            if updateType == "Latest" {
                startArmUpdate(withFilePath: DropBoxFirmwareManager.sharedInstance.armFilePath!)
            }
            else if updateType == "Development" {
                startArmUpdate(withFilePath: devArmFilePath)
            }
        }
        else {
            //only pic needed to be updated and has been.  Call restart
            AVScope.sharedInstance.getBLEManager().writeFirmwareInstruction(.restart)

        }
        
        
    }

    //MARK: FirmwareVersion Stuff
    // all logic for storing firmware versions and comparing firmware version
    
    
    /*
    // This dictionary will relate the main version number to the pic/arm firmware versions.  It should be manually updated while we are still manually loading files on, but will eventually be stored in coredata locally (or on server) when we use a server to get updates.  
    //String is Pic,Arm  Float is Overall Version number
   
    let firmwareVersionDict : [Float:String] = [1.2:"0.04.00,0.01.01", 1.5:"0.05.00,0.01.02",1.6:"0.06.00,0.01.02",1.7:"0.07.00,0.01.02", 2.2: "0.07.00,0.01.03",2.3: "0.08.00,0.01.03"]
    
    //dictionaries to relate version numbers of pic and arm firmware to the file names UPDATE WHEN THERE IS A NEW FILE ADDED
    let picFirmwareFileDict: [String:String] = ["0.06.00": "Scope01.X-8.production","0.04.00":"Scope01.X.04","0.07.00":"Scope01.X-9.production","0.08.00":"Scope01.X-13.production"]
    let armFirmwareFileDict: [String:String] = ["0.02.00": "applicationPicUpdate","0.01.01":"new_fw_01_01","0.01.03":"application11_18"]
    
    */
    
    
    var newestMainFirmware :String? // main firmware version to update to (read from current firmware . txt)
    var newestPicFirmware : String?
    var newestArmFirmware : String?
   // var firmwareVersions : [String:Float]? // dictionary of previously downloaded firmware versions
    
    
    /// Takes currentfirmwarefile and reads the most recent versions from it.  Updates the newest firmware version variables and checks to see what needs updating
    ///
    /// - parameter atLocalUrl: url where currentfirmwarefile.txt is
    func readCurrentFirmwareFile(atLocalUrl: URL) {
        do {
            let newestFirmwareText = try String(contentsOf: atLocalUrl, encoding: String.Encoding.utf8)
           let lines = newestFirmwareText.components(separatedBy: "\n")
            let versions = lines[1].components(separatedBy: ",")
            newestMainFirmware = versions[0]
            newestPicFirmware = versions[1]
            newestArmFirmware = versions[2]
            
            
            
            //Find scope device to compare version numbers to
            var currentScopeDevice : AVScopeDevice?
            // check to see if there is a current scope device
            if let scopeDevice = AVScope.sharedInstance.getBLEManager().activeScopeDevice {
                currentScopeDevice = scopeDevice
            }
                
            else {  // try to load the most recent scope device
                if let savedScope =  AVScope.sharedInstance.getBLELocalInfo().loadActiveScope() {
                    currentScopeDevice = savedScope
                }
                    // nothing saved - there are no past active scopes
                else {
                    print("there is no saved active peripheral")
                }
            }
            
            
            // Compares firmware versions to see if an update is needed
            if let currentFirmwareString = currentScopeDevice?.info.firmwareRevision {
                let needsUpdate = checkIfFirmwareNeedsUpdate(currentFirmware: currentFirmwareString)
               // if needsUpdate {
                    // checks to make sure there are firmware files availible in dropbox
                    DropBoxFirmwareManager.sharedInstance.checkForFirmwareFiles(withVersion: newestMainFirmware!)

                   
                    
                //}
            }
        }
        catch {print("error reading file")}
        
    }
    
    /// CAlled when it is confirmed that there is new firmware availible (from comparing currentFirmware.txt file) and that the firmware files exist (by reading firmware version folder)
    func newFirmwareAvailible() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: AVScopeFirmwareUpdateAvailible),object: self,userInfo: nil)

    }
    
    
    ////////////
    
    /**
     Determines if firmware is up to date.
      - current firmware - string of Pic,Arm version "x.xx.xx,y.yy.yy" - firmware on Scope
      - Returns  - true if it needs an update
 */
    func checkIfFirmwareNeedsUpdate(currentFirmware: String) -> Bool {
        if newestPicFirmware != nil && newestArmFirmware != nil {
        let latestFirmwareString = newestPicFirmware! + newestArmFirmware!
        if latestFirmwareString == currentFirmware {
            return false
        }
        return true
    }
        return false // no update because couldn't read text file
    }
    
    /**
     determines what needs to be updated. returns true if one needs to be updated
     takes - currentFirmware - the firmware loaded on the scope in form form Pic,Arm (x.xx.xx,y.yy.yy)
     returns booleans determining which processors need updating
 */
    func checkWhatNeedsUpdating(currentFirmware: String) -> (picNeedsUpdate: Bool, armNeedsUpdate: Bool) {
    
        //get individual firmware strings
        let currentVersions = splitFirmwareString(firmwareString: currentFirmware)
        
        
        print("currentPic = \(currentVersions.PicString), newPic  = \(newestPicFirmware!)")
        print("currentArm = \(currentVersions.ArmString), newPic  = \(newestArmFirmware!)")

        // check if the versions match
        var picNeedsUpdate = true
        var armNeedsUpdate = true
        if newestPicFirmware! == currentVersions.PicString {
            picNeedsUpdate = false
        }
        if newestArmFirmware! == currentVersions.ArmString {
            armNeedsUpdate = false
        }
      
        return (picNeedsUpdate,armNeedsUpdate)
    }
    
    
    
    
    /*
    /**
     Gets the latest arm firmware based on the newest version in versiondict and the file name in the armfirmwarefiledict
 */
    func getLatestArmFirmware()-> DFUFirmware {
        //get file name
        let latestFirmwareString = firmwareVersionDict[getLatestFirmwareVersion()]
       let (_ , armFirmwareVersion) = splitFirmwareString(firmwareString: latestFirmwareString!)
        let armFirmwareFile = armFirmwareFileDict[armFirmwareVersion]

        //get url
        let armURL =   Bundle.main.url(forResource: armFirmwareFile, withExtension: "zip")!
        //return DFU firmware onject
        return DFUFirmware(urlToZipFile: armURL)!
    }
    */
    
    /*
    /**
     Gets the latest pic firmware URL based on the newest version in versiondict and the file name in the picfirmwarefiledict
     */
    func getLatestPicFirmwareURL() -> URL {
        //get file name
        let latestFirmwareString = firmwareVersionDict[getLatestFirmwareVersion()]
        let (picFirmwareVersion , _) = splitFirmwareString(firmwareString: latestFirmwareString!)
        let picFirmwareFile = picFirmwareFileDict[picFirmwareVersion]

        //return url
        return Bundle.main.url(forResource: picFirmwareFile, withExtension: "hex")!
    }
    */
    
    /**
     Splits a firmware string with Pic and arm versions into individual pic and arm version strings 
     takes - firmware string of form Pic,Arm (x.xx.xx,y.yy.yy)
     returns - Pic String (x.xx.xx)
     returns - Arm String (y.yy.yy)
 */
    func splitFirmwareString(firmwareString: String) -> (PicString: String, ArmString: String) {
        let splitString = firmwareString.components(separatedBy: ",")
        return (splitString[0],splitString[1])
    }
    
    /*
    /**
     gets the latest firmware version from the firmwareversiondict (returns main version num).  Assumes highest version number is newest
 */
    func getLatestFirmwareVersion() -> Float {
        var maxVersion: Float = 0.0
        let mainVersions = firmwareVersionDict.keys
        for version in mainVersions {
            if version > maxVersion {
                maxVersion = version
            }
        }
        return maxVersion
    }
 */
    

  
    
    
    
}

extension String {
    
    /// Create `Data` from hexadecimal string representation
    ///
    /// This takes a hexadecimal representation and creates a `NSData` object. Note, if the string has any spaces or non-hex characters (e.g. starts with '<' and with a '>'), those are ignored and only hex characters are processed.
    
    // this is used for conversion of the Pic Firmware files 
    ///
    /// - returns: Data represented by this hexadecimal string.
    
    func dataFromHexadecimalString() -> Data? {
        let data = NSMutableData(capacity: characters.count / 2)
        
        let regex = try! NSRegularExpression(pattern: "[0-9a-f]{1,2}", options: .caseInsensitive)
        regex.enumerateMatches(in: self, options: [], range: NSMakeRange(0, characters.count)) { match, flags, stop in
            let byteString = (self as NSString).substring(with: match!.range)
            var num = UInt8(byteString, radix: 16)
            data?.append(&num, length: 1)
        }
        
        return data as Data?
    }
}
