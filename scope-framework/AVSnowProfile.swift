//
//  AVSnowProfile.swift
//  scope-framework
//
//  Created by Olivier Brand on 3/1/16.
//  Copyright © 2016 Olivier Brand. All rights reserved.
//


// WHAT??? AUTO MADE FROM CONVERSION TO SWIFT 3???
import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


/// The AVSnowProfile represents data captured after probing Scope in the snow.  Contains both info from device as well as user editablt info that should be associated with a profile

open class AVSnowProfile: NSObject {
    
    
 
    //MARK: Test Initialization
    // functions to create fake profiles for testing 
    
    convenience init(fakeProcessed: Int, PreviousTime: Int ) {
       self.init(fakeProcessed: fakeProcessed, ser: "00114", lat: 22.112, long: 33.421, PreviousTime:  PreviousTime)
    }
    
    init(fakeProcessed: Int, ser: String, lat: Float, long: Float, PreviousTime: Int) {
        testNum = fakeProcessed
        serialNum = ser
        transferTime = Date(timeIntervalSinceNow: -Double(PreviousTime + fakeProcessed * 60) )
        collectionTime = Date()
        batteryCapacity = 89
        forceCalibration = [2,3,21,31,41,21]
        opticalCalibration = 21
        temperature = 38
        latitude = lat
        longitude = long
        accelFS = 212
        gyroFS = 32
        PICFirmwareVersion = "2.3"
        ARMFirmwareVersion = "4.2"
        mainPCBRev = 2
        NRFPCBRev = 3
        slope = 23
        testTime = 3.0
        errorCode = 0
        profileDepth = 143
        isNew = true
        isDraft = false
        isSubmitted = false
        
        
        //make random profiles
        let avgNumOfLayers = 10
        let avgTotalDepth = 1200 // mm
        let avgLayerThickness = avgTotalDepth/avgNumOfLayers
        
        var temphardnesses = [Float]()
        
        let randomNumOfLayers = arc4random_uniform(UInt32(avgNumOfLayers*2))
        //print("randomNumOfLayers = \(randomNumOfLayers)")
        for _ in 0..<randomNumOfLayers+1 {
            let randomLayerThickness = arc4random_uniform(UInt32(avgLayerThickness*2))
            let randomHardness = arc4random_uniform(400) // max hardness??
            for _ in 0..<randomLayerThickness {
                temphardnesses.append(Float(randomHardness))
            }
        }
        
        
        /*
        // make standard Profiles (use to check force conversion)
         var temphardnesses = [Int]()
         var layerHardnesses = [0,16,80,200,400,640]
        for i in 0..<6 {
            for _ in 0..<250 {
                temphardnesses.append(layerHardnesses[i])
            }
            
        }
        */
        
       // print ("hardness l = \(temphardnesses.count)")
        hardness = temphardnesses
        
        
        //rawData = NSData()
        let randomNames = ["Superior Ridge", "Patsy Marely", "Wolverine NE", "10420 Trees", "Days Gully", "Beartrap  Drift","Rocky Pt", "Edleweiss Top", "Glory Bowl", "Turkey Chute", "Dog Ridge","Loveland SW", "Rolleston E","Hot Pocket", "Cragieburn W Ridge", "Little AK", "Shot 2 top", "Olympus E Gully", "Cold Fusion W", "Argenta Tree", "Profile 32","Profile 12", "Profile 123", "Profile 21", "Profile 87","Profile 56","Profile 4"]
        let randomNameNum = arc4random_uniform(UInt32(randomNames.count - 1))
        name = randomNames[Int(randomNameNum)]
        
    }
    
    
 
    //MARK:-  Header Data with every profile (sent from Device)  DO NOT NEED TO BE VAR AND OPTIONAL _ CONSIDER CHANGING TO LET AND HARDCODNG INIT FOR SPEED??
    
    // Environmental
    var temperature: Int?
    
    // Test specific
    var testNum: Int?
    var batteryCapacity: Int?
    var collectionTime: Date? // Date/Time when profile was created
    var testTime: Float?
    var errorCode: Int?
    var profileDepth : Int?

    //device specific
    var accelFS: Int?
    var gyroFS: Int?
    var forceCalibration: [Int]?
    var opticalCalibration: Int?
    
    //versions and revisions
    var serialNum: String?
    var PICFirmwareVersion: String?
    var ARMFirmwareVersion: String?
    var mainPCBRev: Int?
    var NRFPCBRev: Int?
    
    //changeable 
    var isNew : Bool?
    var isSubmitted : Bool?
    var isDraft : Bool?
    
    
    //MARK:  Added Header Data
    var transferTime: Date? //Date/Time when location was transfered to device

    
    
    //MARK: Info Auto added by phone
    var latitude: Float?
    
    var longitude: Float?
    
    
    //MARK: Processed Data with every profile
    var hardness: [Float]? // in kPa, 1 point per mm.  Length of this array is the depth of the profile
    
    
    
    //MARK: User Added Info (all optional)
    var annotations : ProfileAnnotations? // see profile annotation class - holds user inputted annotations
    
    var locationName : String? // name of location
    
    var freeText : String? // free text comments
    
    var aspect : String? // aspect of area that was probed
    
    var slope : Int? // slope of area that was probed
    
    var totalDepth : Int? // total depth in cm
    
    //var photos : [UIImage]? // array of images
    
    var name : String? // custom profile name
    
    
    
    
    
    //MARK: Raw/Debug Data (optional) - probably don't need?
   // let rawData: NSData // holds data that was transferred (can be raw or processed)
    
     var force: [Int]?
     var depth: [Int]?
     var accelerometer: Accelerometer?
     var gyro: Gyro?
     var optical: Optical?
   
     var circularBufferHead: Int?
    
    
    //MARK - Real Initilization
    
    /**
     initializes a blank snowprofile placeholder object to display for raw data collection
     
     - returns: a blank snowprofile object with only a testnumber and serialNumber
     */
    init(placeHolderForRaw testNumber: Int, serNum: String) {
        testNum = testNumber
        serialNum = serNum
    }

    
    /**
     Creates new AVSnowProfile Object with RawData from BLE.  Dynamically creates header file structure lengths based on 32 bit alignment
     
     - parameter data: snow profile data from BLE (see snow profile structure spec)
     
     - returns: AVSnowProfile Object
     */
    init(withBLEData data: Data) {
        
        
        // gets indicies and the order array from snowProfile Service
       let (indicies,_) = AVSnowProfileService.sharedInstance.getIndiciesAndLength()
        let order = AVSnowProfileService.sharedInstance.order
       
        
        //loop through all header data types
        
        for i in 0..<order[0].count {
            let length = order[1][i] as! Int
            // switch on the string names of the header data types, convert as necessary
            print(" converting data for \(order[0][i]), start index is \(indicies[i]), range  is \(length)")
            
            let currentMetaDataType = order[0][i] as! String
            switch currentMetaDataType {
            case "test time" :  
                // Float32
                testTime = data.convertFromFloat(indicies[i], range: length)[0]
                if (testTime?.isNaN)! {
                    testTime = nil
                }
                
            case "error code" :
                //UINT8
                errorCode = data.convertFromInt8(indicies[i], range: length)[0]
            case "temp":
                // INT8
                temperature = data.convertFromSignedInt8(indicies[i], range: length)[0]
                
            case "testNum":
                //UINT 16
                testNum = data.convertFromInt16(indicies[i], range: length)[0]
                
            case "battery":
                //UINT8
                batteryCapacity = data.convertFromInt8(indicies[i], range: length)[0]
                
            case "accel":
                //UINT 8
                accelFS = data.convertFromInt8(indicies[i], range: length)[0]
                
            case "gyro":
                //UINT 16
                gyroFS = data.convertFromInt16(indicies[i], range: length)[0]
                
            case "force":
                //[UINT16]
                forceCalibration = data.convertFromInt16(indicies[i], range: length)
                
            case "profileDepth":
                // UINT 16 
                profileDepth = data.convertFromInt16(indicies[i], range: length)[0]
                
            case "optic":
                //UINT16
                opticalCalibration = data.convertFromInt16(indicies[i], range: length)[0]
                
            case "serial":
                //String
                let longSerialNum = data.convertFromChars(indicies[i], range: length)
                serialNum = longSerialNum.substring(to: longSerialNum.characters.index(longSerialNum.startIndex, offsetBy: 5)) // ignore anything after first 5
            case "PIC":
                //String
                PICFirmwareVersion = data.convertFromChars(indicies[i], range: length)
                
                
            case "ARM":
                //String
                ARMFirmwareVersion = data.convertFromChars(indicies[i], range: length)
                
            case "mainPCB":
                //UInt8
                mainPCBRev = data.convertFromInt8(indicies[i], range: length)[0]
                
            case "NRFPCB":
                //UINT8
                NRFPCBRev = data.convertFromInt8(indicies[i], range: length)[0]
                
            case "latitude":
                //Float32
                let testLat = data.convertFromInt8(indicies[i], range: length)
                print("testLong(uint8) = \(testLat)")
                latitude = Float(data.convertFromFloat(indicies[i], range: length)[0])
                if (latitude?.isNaN)! {
                    latitude = nil
                }
                
            case "longitude":
                // Float32
                let testLong = data.convertFromInt8(indicies[i], range: length)
                print("testLong(uint8) = \(testLong)")
                longitude = Float(data.convertFromFloat(indicies[i], range: length)[0])
                if (longitude?.isNaN)! {
                    longitude = nil
                }
                
            default:
                print("invalid header type to convert = \(order[0][i])")
            }
            
        }
        
        // convert rest of data 
           print("indicies = \(indicies)")
           let hardnessRange = profileDepth! - 1 //data.count - indicies.last! //length of hardness data (all remaining data) //JOE: notall remaining data. need to exclude the remainder, i.e. whatever we round up by to get to 20 multiple
           print("hardness range = \(hardnessRange)")
        if hardnessRange > 0 && data.count > hardnessRange + indicies.last! {  // check to prevent crash caused by potential mismatch of profile depth in metadata and actual profile depth. Also checks to make sure there is hardness data
            
            let tempHardness = data.convertFromInt8(indicies.last! + 1 , range: hardnessRange)
            hardness = AVSnowProfileService.sharedInstance.HH51_to_kPa(hardnessArray: tempHardness)

        }
        else {
            hardness = [0.0]

        }
        
        // add transfer time
        transferTime = Date() //NSDateFormatter.localizedStringFromDate(NSDate(), dateStyle: .MediumStyle, timeStyle: .ShortStyle)
        
        //set as new 
        isNew = true
        isSubmitted = false
        isDraft = false

        
    }
    
 
    /**
     Creates AVSnowProfile Object with contents of JSON
     
     - parameter data: NSData object that is of JSON Form
     
     - returns: AVSnowProfile Object
     */
    init (withJSONData dictionary: [String:AnyObject]) {
        
        // get dateFormatter
        let dateFormatter = AVCoreDataManager.sharedInstance.getDateFormatter()
        
        // non optional stuff
        testNum = dictionary["testNum"] as! Int!
        let longSerialNum = dictionary["serialNum"] as! String!
        serialNum = longSerialNum?.substring(to: (longSerialNum?.characters.index((longSerialNum?.startIndex)!, offsetBy: 5))!) // ignore anything after first 5 (this should be ok in future but will fix for old bad data)
        
        //testTime =
        errorCode = dictionary["errorCode"] as! Int!
        transferTime  =  dateFormatter.date( from: dictionary["transferTime"] as! String)!
        //collectionTime =  dateFormatter.dateFromString( dictionary["collectionTime"] as! String)!
        batteryCapacity = dictionary["batteryCapacity"] as! Int!
        opticalCalibration = dictionary["opticalCalibration"] as! Int!
        temperature = dictionary["temperature"] as! Int!
        profileDepth = dictionary["profileDepth"] as! Int!
        hardness = dictionary["hardness"] as! [Float]
        forceCalibration = dictionary["forceCalibration"] as! [Int]!
        accelFS = dictionary["accelFS"] as! Int!
        gyroFS = dictionary["gyroFS"] as! Int!
        PICFirmwareVersion = dictionary["PICFirmwareVersion"] as! String!
        ARMFirmwareVersion = dictionary["ARMFirmwareVersion"] as! String!
        mainPCBRev = dictionary["mainPCBRev"] as! Int!
        NRFPCBRev = dictionary["NRFPCBRev"] as! Int!
        isSubmitted = dictionary["isSubmitted"] as! Bool!
        isNew = dictionary["isNew"] as! Bool!
        isDraft = dictionary["isDraft"] as! Bool!
        
        
        // optional stuff
        if let newLatitude = dictionary["latitude"] as? Float {
            latitude = newLatitude
        }
        if let newLongitude = dictionary["longitude"] as? Float {
            longitude = newLongitude
        }
        if let newAnnotations = dictionary["annotations"] as? [String:[AnyObject]]{
            if annotations == nil {
                annotations = ProfileAnnotations() // creates object so it works
            }
            for i in 0..<newAnnotations["depth"]!.count {
                annotations?.addComment(newAnnotations["depth"]![i] as! Float, thickness: newAnnotations["thickness"]![i] as! Float, comment: newAnnotations["text"]![i] as! String, importantFlag: newAnnotations["flag"]![i] as! Bool)
                
            }
            
        }
        if let newAspect = dictionary["aspect"] as? String {
            aspect = newAspect
        }
        if let newSlope = dictionary["slope"] as? Int {
            slope = newSlope
        }
        if let newName = dictionary["name"] as? String {
            name = newName
        }
        if let newTotalDepth = dictionary["totalDepth"] as? Int {
            totalDepth  = newTotalDepth
        }
        if let newLocationName = dictionary["locationName"] as? String {
            locationName = newLocationName
        }
        if let newFreeText = dictionary["freeText"] as? String {
            freeText = newFreeText
        }
    }
    
    
    // MARK: Methods
    
    
    /// Converts the AVSnowProfile into a single string meant for CSVexport
    ///
    /// - Returns: a string of CSV type
    func createCSVString() -> String {
 
        
        
        // make each metadata field a string with default values
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .long
        let testNumberString = String(testNum ?? 0)
        var  transferTimeString = "N/A"
        if transferTime != nil {
            transferTimeString = dateFormatter.string(from: transferTime!)
        }
        let latitudeString = String(latitude ?? 0.0)
        let longitudeString =  String(longitude ?? 0.0)
        let testTimeString = String(testTime ?? 0.0)
        let batteryCapacityString = String(batteryCapacity ?? 0)
        let profileDepthString = String(profileDepth ?? 0)
        let errorCodeString = String(errorCode ?? 0 )
        let accelFSString = String(accelFS ?? 0)
        let gyroFSString = String(gyroFS ?? 0)
        var forceCalString = "N/A"
        if forceCalibration != nil {
            forceCalString = ""
            for i in forceCalibration! {
                forceCalString += (String(i) + "_")
            }
        }
        let opticalCalString = String(opticalCalibration ?? 0)
        let mainPCBRevString = String(mainPCBRev ?? 0)
        let NRFPCBRevString = String(NRFPCBRev ?? 0 )
        let temperatureString = String(temperature ?? 0 )
     
        
        
        let metaData = ["Serial_Number " + serialNum!,
                        "Test_Number " + testNumberString,
                        "Test_Time " + testTimeString,
                        "Transfer_Time " + transferTimeString,
                        "Latitude " + latitudeString,
                        "Longitude " + longitudeString,
                        "Battery_Capacity " + batteryCapacityString,
                        "Profile_Depth " + profileDepthString,
                        "Error_Code " + errorCodeString,
                        "AccelFS " + accelFSString,
                        "GyroFS " + gyroFSString,
                        "Force_Cal " + forceCalString,
                        "Optical_Cal " + opticalCalString,
                        "Main_PCB_Rev " + mainPCBRevString,
                        "NRF_PCB_Rev " + NRFPCBRevString,
                        "Temperature " + temperatureString]
        
        //create metadata string
        var CSVString = ""
        
        for i in metaData {
            CSVString += (i + "\n")
        }
        

        CSVString += ("\n \n hardness(kPa), 1 pt per mm depth \n")

        
        for i in self.hardness! {
            let rounded = round(i*10)/10
            CSVString += (String(rounded) + "\n")
        }
        return CSVString
    }
    
    
    /// checks if the
    /// Checks if the transfer time associated with the profile is still recent enough to justify the "NEW" title
    ///
    /// - Returns: true if it still should be new, false if it has "expired"
    func checkNewTime()->Bool {
        let expirationTime = 1200 // time in sec till expire
        let timeGap = Int((self.transferTime!.timeIntervalSinceNow))
        
        if abs(timeGap) < expirationTime {
            return true
        }
        else {
            return false
        }
        
        
    }
    
    
    /**
     Converts the hardness array in the snowprofile into an array that is scaled to the screen width (0 -100).  This conversion is based on a quadratic fit to the International classification of snow on the ground and an assumption to display the mean hardnesses of each hand hardness level linearly spaced (with half of each spacing at beginning and 3/4 at end)
 **/
    func convertToQuadraticFit() -> [Int] {
        let hardnessArray = self.hardness!
        var convertedHardnesses = [Int]()
        for i in hardnessArray {
           let hardnessDouble = Double(i)
           let scaledHardness = -0.000229 * pow(hardnessDouble,2) + (0.2873 * hardnessDouble) + 8.4 
            
            // make sure the conversion doesn't give a number off screen - if it does, just give it the max value
            if scaledHardness < 100 {
            convertedHardnesses.append(Int(scaledHardness))
            }
            else {
                convertedHardnesses.append(100)
            }
        }
        return convertedHardnesses
    }
    
    
    /// Converts the hardness array in the snowprofile into an array that is bassed on the ICSSG equation, then scaled to fit graph 
    ///
    /// - returns: converted Array of hardness (0 - 100 assuming width is 100) - shoudl 
    func convertToICSSGeq() -> [Int] { // equation derived from International classification for snow on ground (pg 43), then scaled to fit on graph so 0 hardness is 5% of way across and P is 87.5%
        let hardnessArray = self.hardness!
        var convertedHardnesses = [Int]()
        for i in hardnessArray {
            let hardnessDouble = Double(i)
            let scaledHardness = 6.61 * pow(hardnessDouble,(1/2.4))+2.48   //7.05*pow(hardnessDouble,(1/2.4))+2.315
            
            
            if scaledHardness < 100 {
                convertedHardnesses.append(Int(scaledHardness))
            }
            else { // maxed out (don't want anything over 100)
                convertedHardnesses.append(100)
            }
        }
        return convertedHardnesses
    }

    
 
    //MARK: Structures
    
    
    /**
     *  Force calibration structure.  Contains Calibration constants  - MAYBE DONT NEED?!?!
     */
    struct ForceCalibrationStruct {
        let A: Int
        let B: Int
        let C: Int
        let D: Int
        let E: Int
        
        init(A:Int,B:Int,C:Int,D:Int, E: Int) {
            self.A = A
            self.B = B
            self.C = C
            self.D = D
            self.E = E
        }
    }
    
    /**
     *  Accelerometer Data Structure - contains X,Y,and Z acceleration in (units)
     */
    struct Accelerometer {
        let x: [Int]
        let y: [Int]
        let z: [Int]
        init(X:[Int],Y:[Int],Z:[Int]){
            self.x = X
            self.y = Y
            self.z = Z
        }
    }
    
    /**
     *  Gyro Data structure - contains yaw, pitch, and roll in (units)
     */
    struct Gyro {
        let yaw: [Int]
        let pitch: [Int]
        let roll: [Int]
        init(Yaw:[Int],Pitch:[Int],Roll:[Int]){
            self.yaw = Yaw
            self.pitch = Pitch
            self.roll = Roll
        }
    }
    
    /**
     *  Optical Data Structure - contains optical x (units), optical y (units), and squall.
     */
    struct Optical {
        let x: [Int]
        let y: [Int]
        let squal: [Int]
        init(X:[Int],Y:[Int],Squal:[Int]){
            self.x = X
            self.y = Y
            self.squal = Squal
        }
    }
    
    
    
}

// Represents a profileAnnotations object.  This holds all information relating to annotations on a specific profile (max 1 object per profile)

class ProfileAnnotations {
    
    /// Property holding all of the annotations
    var comments = [Annotation]()
    
    
    /**
     Adds an additional annotation to the profileAnnotations object
     
     - parameter depth:         location for comment (depth from surface of profile) (INT)
     - parameter comment:       comment (String)
     - parameter importantFlag: Bool indicating if the annotation should have a flag
     */
    func addComment(_ depth: Float, thickness: Float, comment: String?,importantFlag: Bool?) {
        
        let newAnnotation = Annotation(commentDepth:depth, newThickness: thickness, comment: comment, important: importantFlag)
        comments.append(newAnnotation)
    }
    
    /**
     Deletes the given comment
     */
    func deleteComment(_ comment: Annotation) {
        
        let closestAnnotationIndex = comments.index(where: {$0.depth == comment.depth})
        comments.remove(at: closestAnnotationIndex!)
        
    }
    /*
    /**
     Edits the closest annotation to the given depth
     
     - parameter closestToDepth: int value of depth that comment is closest to
     - parameter newDepth:       new depth value (int)
     - parameter newtext:        new text value (string)
     - parameter newFlag:        new flag value (bool)
     */
    func editComment(closestToDepth: Float, newDepth: Float, newtext: String, newFlag: Bool) {
        let closestAnnotation = findClosestAnnotation(closestToDepth)
        let closestAnnotationIndex = comments.indexOf({$0.depth == closestAnnotation.depth})
        
        
        comments[closestAnnotationIndex!].depth = newDepth
        comments[closestAnnotationIndex!].text = newtext
        comments[closestAnnotationIndex!].flag = newFlag
    }
    */
    /**
     Returns annotation in comments that is closest to the given depth
     
     - parameter closestToDepth: int value of depth that should be found
     
     - returns: Annotation that is closest to depth  (or nil, if not within tolerance)
     */
    func findClosestAnnotation(_ closestToDepth: Float) -> Annotation? {
        if comments.count > 0 {
        var closestAnnotation = comments[0]
        var minDifference : Float?
        for annote in comments {
            let compareDifference = abs(closestToDepth - closestAnnotation.depth)
            let currentDifference = abs(closestToDepth - annote.depth)
            // check if next comment is closer to given location than previous
            if currentDifference < compareDifference {
                closestAnnotation = annote
                minDifference = currentDifference
            }
                    }
       // - check if minimum difference is within x cm
        if minDifference < 10 {
            print("min difference = \(minDifference)")
        return closestAnnotation
        }
        print("no annotations found within bounds")
        }
        // if check fails (tap wasn't close enough to justify a selection) (or there were no comments)
        return nil
        
      
    }
    
    
    
  

 
    
    
    /**
     *  Annotation structure, includes  a comment, a location for the comment, and a flag to indicate if the comment is important
     */
    struct Annotation {
        var depth: Float //depth from surface (cm) of layer to annotate
        var thickness: Float //thickness of layer
        var text: String? //user inputted comments on layer
        var flag: Bool? //flag on annotation (flags are for dangerous/important comments)
        
        init(commentDepth: Float, newThickness: Float, comment: String?, important: Bool?) {
            self.depth = commentDepth
            self.thickness = newThickness
            self.text = comment
            self.flag = important
        }
    }
}


