//
//  AVBLELocalInfo.swift
//  scope-framework
//
//  Created by Garrett Harmsen on 7/18/16.
//
//
// Handles all CORE DATA methods for saving/loading of scope devices

import Foundation
import CoreData
import UIKit

open class AVBLELocalInfo: NSObject {
    static let sharedInstance = AVBLELocalInfo()
    
    
    //MARK: Strings for Core Data
    let firmwareAttribute = "firmwareRevision"
    let hardwareAttribure = "hardwareRevision"
    let manufacturerIDAttribute = "manufacturerID"
    let manufacturerNameAttribute = "manufacturerName"
    let modelNumberAttribute = "modelNumber"
    let nameAttribute = "name"
    let orgIDAttribute = "orgID"
    let serialAttribute = "serialNumber"
    let ScopeDeviceEntity = "ScopeDevice"
    let activeAttribute = "active"
    let minTestTransAttribute = "minTestNumToTransfer"
   
    //MARK: Core DATA
    
    /**
     gets managedContext
     
     - returns: managedContext
     */
    func getManagedContext() -> NSManagedObjectContext {
        let appDelegate =
            UIApplication.shared.delegate as! AppDelegate
        return appDelegate.managedObjectContext
    }
    /**
     Saves a scopeDevice, if there is a scope already saved with the same serial number, it will delete that one first so there is not a repeat
     
     - parameter scopeToSave: AVScopeDevice to Save
     */
    func saveScopeDevice(_ scopeToSave: AVScopeDevice) {
        
        //deletes the saved scope with the same serial number (if applicable)
        deleteObjectWith(serialNumber: scopeToSave.info.serialNumber)
     
        
        let managedContext = getManagedContext()
        
        // creates new entity
        let entity =  NSEntityDescription.entity(forEntityName: ScopeDeviceEntity,
                                                        in:managedContext)
        
        let newEntity = NSManagedObject(entity: entity!,
                                            insertInto: managedContext)
        print("saving scope named \(scopeToSave.name), as active = \(scopeToSave.active)")
        
        //sets data to new entity
        newEntity.setValue(scopeToSave.name, forKey: nameAttribute)
        newEntity.setValue(scopeToSave.info.firmwareRevision, forKey: firmwareAttribute)
        newEntity.setValue(scopeToSave.info.hardwareRevision, forKey: hardwareAttribure)
        newEntity.setValue(scopeToSave.info.manufacturerName, forKey: manufacturerNameAttribute)
        newEntity.setValue(scopeToSave.info.modelNumber, forKey: modelNumberAttribute)
        newEntity.setValue(scopeToSave.info.serialNumber, forKey: serialAttribute)
        newEntity.setValue(scopeToSave.info.systemID.manufacturerId, forKey: manufacturerIDAttribute)
        newEntity.setValue(scopeToSave.info.systemID.organizationallyUniqueId, forKey: orgIDAttribute)
        newEntity.setValue(scopeToSave.active, forKey: activeAttribute)
        newEntity.setValue(scopeToSave.minTestToTransfer, forKey: minTestTransAttribute)
    
        do {
            try managedContext.save()
           
            
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
        catch {
            print("other error")
        }

         print("saved")
    }
    
    
    /**
     Loads array of scope names
     
     - returns: array of all known scope names
     */
    func loadScopeNames() -> [String]{
        let allScopes = loadScopes()
        var scopeNameArray = [String]()
        for scope in allScopes {
            scopeNameArray.append(scope.name!)
        }
        return scopeNameArray
    }
      
    
    /**
     Loads scope device object that is active
     
     - returns: active scope
     */
    func loadActiveScope() -> AVScopeDevice? {
        let allScopes = loadScopes()
        for scope in allScopes {
            if scope.active {
               // print("active scope is \(scope.name!)")
                return scope
            }
        }
        print("no active scope")
        return nil
       
    }
    
    /**
     Loads all scope devices
     
     - returns: array of AVScope Devices
     */
    func loadScopes() -> [AVScopeDevice] {
        let managedContext = getManagedContext()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: ScopeDeviceEntity)
        
        var scopeDevices = [AVScopeDevice]()
        do {
            let scopeDeviceEntities = try managedContext.fetch(fetchRequest) as! [NSManagedObject]
            
            for i in scopeDeviceEntities {
                let newScopeInfo = AVDeviceInfo(hardwareRevision: i.value(forKey: hardwareAttribure) as! String, serialNumber: i.value(forKey: serialAttribute) as! String, modelNumber: i.value(forKey: modelNumberAttribute) as! String, manufacturerName: i.value(forKey: manufacturerNameAttribute) as! String, firmwareRevision: i.value(forKey: firmwareAttribute) as! String, systemID: AVSystemId(manufacturerId: i.value(forKey: manufacturerIDAttribute) as! UInt, organizationallyUniqueId: i.value(forKey: orgIDAttribute) as! UInt))

                let newScopeDevice = AVScopeDevice(deviceInfo: newScopeInfo, name: i.value(forKey: nameAttribute) as! String, active: i.value(forKey: activeAttribute) as! Bool)
                // adds new scope device to array
                scopeDevices.append(newScopeDevice)
            }
            
        }catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return scopeDevices
    }
    
    
    /**
     Checks to see if there is a saved minimum test number to transfer for the device, if there is, return it, otherwise, return 0
     - parameter forScopeSN: serial number of device to check 
     - returns: Integer showing the minimum test number that the device will auto transfer
 */
    func getMinimumTestNumToTrans(forScopeSN: String) -> Int {
        let managedObjectContext = getManagedContext()
        
        //get shortened SerNum
        let index = forScopeSN.characters.index(forScopeSN.startIndex, offsetBy: 5)
        let serialNumDigits = forScopeSN.substring(to: index)
        
        
        //let predicate = NSPredicate(format: serialAttribute + " == " + serialNumDigits) // predicate to match SNs
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: ScopeDeviceEntity)

       // fetchRequest.predicate = predicate
        
        do {
            if let fetchedEntities = try managedObjectContext.fetch(fetchRequest) as? [NSManagedObject] {
                //print("numberofEntities =  \(fetchedEntities.count)")
                for i in fetchedEntities {
                    let savedSerNum = i.value(forKey: serialAttribute) as! String
                    if savedSerNum == forScopeSN {
                        if let savedMinTestToTrans = i.value(forKey: minTestTransAttribute) as? Int {
                            return savedMinTestToTrans
                        }
                    }
                    
                }
            }
        } catch {
            print("no previously saved entity with matching SN")
        }
        
        // if no saved min test num for this scope
        return 0
    }
    
    
    
    
    /**
     Deletes the previously active object
     - parameter withSerialNumber: serial number string
     - parameter fromEntity:       entity type to delete
     */
    func deleteActiveObject() {
        let managedObjectContext = getManagedContext()
        let predicate = NSPredicate(format: "active == true")
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: ScopeDeviceEntity)
        
        fetchRequest.predicate = predicate
        
        do {
            if let fetchedEntities = try managedObjectContext.fetch(fetchRequest) as? [NSManagedObject] {
                if let entityToDelete = fetchedEntities.first {
                    managedObjectContext.delete(entityToDelete)
                }
            }
        } catch {
            print("no previously saved entity to delete")
            // Do something in response to error condition
        }
        
        
        do {
            try managedObjectContext.save()
        } catch {
            // Do something in response to error condition
        }
    }
    
    
    /**
     Deletes the object with a matching serial number (if there is one)
     - parameter withSerialNumber: serial number string
     - parameter fromEntity:       entity type to delete
     */
    func deleteObjectWith(serialNumber: String) {
        let managedObjectContext = getManagedContext()

        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: ScopeDeviceEntity)
        
        do {
            if let fetchedEntities = try managedObjectContext.fetch(fetchRequest) as? [NSManagedObject] {
                for i in fetchedEntities {
                    let savedSerNum = i.value(forKey: serialAttribute) as! String
                    if savedSerNum == serialNumber {
                        managedObjectContext.delete(i)
                    }
                }
            }
        } catch {
            print("no previously saved entity to delete with ser Num \(serialNumber)")
            // Do something in response to error condition
        }
        do {
            try managedObjectContext.save()
        } catch {
            print("could not save")
            // Do something in response to error condition
        }
        print("deleted with SN \(serialNumber)")
        
    }
    
}
