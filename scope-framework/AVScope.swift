//
//  AVScope.swift
//  scope-framework
//
//  Created by Olivier Brand on 2/25/16.
//  Copyright © 2016 Olivier Brand. All rights reserved.
//
// Holds public enums and entry point to scope framework (AVScope)

import UIKit
import CoreBluetooth

//MARK: - Enums

// Defines Scope Connection Errors -- not used, probably don't need this
public enum ScopeConnectionError : Error {
  
  /**
  *  Scope the app tried to connect could not be found
  *
  *  @return notFound error
  */
  case notFound
  
  /**
   *  Scope is already connected
   *
   *  @return alreadyConnected error
   */
  case alreadyConnected
  
  /**
   *  Connection did not succeed
   *
   *  @param  an error description
   *
   *  @return cannotConnect error
   */
  case cannotConnect(String)
  
  // Any other critical errors
  /**
  *  Any critical error
  *
  *  @return critical error
  */
  case critical
}



/**
 The AVDeviceStatus is used to change the Scope device state and as well to get the current state from the Scope device
 
 - Ready:            The device is ready to perform any operations
 - Probing:          The device is actually probing
 - Processing:       The device is processing a snow profile
 - TransferringData: The device is transferring data
 - CriticalError:    The device has encountered a critical error
 - Slope:            Request a slope from the device (write)
 - FirmwareUpdate:   Request firmware update from the device (write)

 */
@objc
public enum AVDeviceStatus: Int {
  /** Ready: The device is ready to perform any operations
   */
  case ready = 0
  /** Probing: The device is actually probing
   */
  case probing = 1
  /** Processing: The device is processing a snow profile
   */
  case processing = 2
  /** TransferringData: The device is transferring data
   */
  case transferringData = 3
  /** CriticalError: The device has encountered a critical error
   */
  case criticalError = 4
  /** Slope: Request a slope from the device (write)
   */
  case slope = 5
  /** FirmwareUpdate: Request firmware update from the device (write)
   */
  case firmwareUpdate = 6
    
  case disconnected = 7
   /**
     device is aligned and ready to probe
 */
  case aligned = 8
  
  // We protect the enumeration so something like: AVDeviceStatus(10) would return nil
  static var cases : [AVDeviceStatus] = [ready, probing, processing, transferringData, criticalError, slope, firmwareUpdate, disconnected, aligned]
  
  /**
   Constructor that can be used when the code is returned from Scope
   
   - parameter status: Device status as an integer
   
   - returns: The associated enum value
   */
  public init?(status:Int) {
    if !(0...8).contains(status) {
    return nil
    }
    
    self = AVDeviceStatus.cases[status]
  }
}
/**
 The AVProbingErrorCode enumeration is used to communicate the error code while Scope was probing
 
 - NoError:          No error, all went ok
 - ProbeSpeed:       Speed related error
 - Calibration:      Calibration related error
 - TransferringData: Data transfer error
 - ProbeSmoothness:  Smoothness error
 - SensorFailure:    Sensor has failed
 - Icing:            Ice on the sensor
 - Battery:          Battery issue that prevented the device to probe
 - DataProcessing:   Data processing error
 */
@objc
public enum AVProbingErrorCode: Int {
  /** NoError: No error, all went ok
   */
    case noError = 0
  /** ProbeSpeed: Speed related error
   */
    case probeSpeed = 1
  /** Calibration: Calibration related error
   */
    case calibration = 2
  /** TransferringData: Data transfer error
   */
    case transferringData = 3
  /** ProbeSmoothness: Smoothness error
   */
    case probeSmoothness = 4
  /** SensorFailure: Sensor has failed
   */
    case sensorFailure = 5
  /** Icing: Ice on the sensor
   */
    case icing = 6
  /** Battery: Battery issue that prevented the device to probe
   */
    case battery = 7
  /** DataProcessing: Data processing error
   */
    case dataProcessing = 8
    
    // We protect the enumeration so something like: AVProbingErrorCode(10) would return nil
    static var cases : [AVProbingErrorCode] = [noError, probeSpeed, calibration, transferringData, probeSmoothness, sensorFailure, icing, battery, dataProcessing]
  
  /**
   Constructor that can be used when the code is returned from Scope
   
   - parameter status: Device status as an integer
   
   - returns: The associated enum value
   */
    public init?(status:Int) {
      if !(0...8).contains(status) {
      return nil
      }
      
      self = AVProbingErrorCode.cases[status]
    }
    
}

//MARK: - Notifications

/// Defines the status notification globally
public let AVStatusDidChangeNotification = "com.avatech.scope.statusdidchangenotification"

///  Defines the probing status notification
public let AVProbingStatusNotification = "com.avatech.scope.probingstatusnotification"

///  Defines the availability of snow profiles notification
public let AVSnowProfilesAvailableNotification = "com.avatech.scope.snowprofilesavailablenotification"

 /// Defines the battery level notification
public let AVBatteryLevelNotification = "com.avatech.scope.batterylevelnotification"

/// Defines the slope angle notification
public let AVSlopeAngleNotification = "com.avatech.scope.slopeanglenotification"

/// Defines the connected Notification
public let AVScopeConnectedNotification = "com.avatech.scope.deviceConnected"

/// Defines the Disconnected Notification
public let AVScopeDisconnectedNotification = "com.avatech.scope.deviceDisconnected"

/// Defines the newName Notification
public let AVScopeNamedNotification = "com.avatech.scope.newName"

 /// Defines the transferringNotification
public let AVTransferringNotification = "com.avatech.scope.transferring"

/// Defines the BLE Scope Discovered Notification
public let AVScopeFoundNotification = "com.avatech.scope.found"

///Defines the BLE state notification
public let AVScopeBLEChangedNotification = "com.avatech.scope.ble"

///Defines the HighVolumeProfilesTransferNotification
public let AVScopeHighVolumeProfilesTransferNotification = "com.avatech.scope.highVolProfTrans"

///Defines the FirmwareUpdateAvailible Notification
public let AVScopeFirmwareUpdateAvailible = "com.avatech.scope.firmwareUpdateAvailible"



/// Entry point of the Scope Framework
/// Use of this class allows to get access to the entire framework
/// Get a handle of the Scope service is just: let scopeService = AVScope.sharedInstance
open class AVScope: NSObject {

  /** Singleton
   */
  open static let sharedInstance = AVScope()
  

    //MARK: properties
    
  /// Contains Device info for currently connected scope device
  fileprivate var connectedScopeInfo = AVDeviceInfo(hardwareRevision: "", serialNumber: "", modelNumber: "", manufacturerName: "", firmwareRevision: "", systemID: AVSystemId(manufacturerId: 0, organizationallyUniqueId: 0))
  /// Counts how many fields of connectedScopeInfo have been updated
  fileprivate var connectedScopeInfoFieldCounter = 0
  /// Completion handler for getConnectedDevice
  fileprivate var deviceInfoCompletionMethod:(AVScopeDevice)->Void = {x in x}
  /// Completion handler for getAvailibleScopeDevices
 fileprivate var getAvailibleScopeDevicesCompletionMethod: ([CBPeripheral])->Void = {x in x}
     /// Completion handler for connect to Scope
    fileprivate var connectedScopeCompletionMethod: (Void)-> Void = {x in x}
   
    
    
    //MARK: newData/connection Methods
    
    
    /**
     Called when a scope is connected with all characteristics.  First calls "get connected Device" to get the device info, saves the connected device, checks if it is in firmware update mode or normal mode
     // if it is in normal mode, read profile IDs and ask to sync any profiles that haven't been synced.
     */
    open func didConnectWithCharacteristics() {
        
        getConnectedDevice({connectedDevice in
            
            // make old connected scope not active and save, also deletes the active saved instance of this device
            if let oldConnectedDevice = self.getBLEManager().activeScopeDevice {
                oldConnectedDevice.active = false
                AVBLELocalInfo.sharedInstance.saveScopeDevice(oldConnectedDevice)
            }
            
            // make new connected scope active
            connectedDevice.active = true
            self.getBLEManager().activeScopeDevice = connectedDevice
            self.getBLEManager().readService("Device Status")
            
            
            //save/overwrite
            print("active scope name = \(self.getBLEManager().activeScopeDevice?.name!)")
            print("profileTransferDisable  = \(AVSnowProfileService.sharedInstance.profileTransferDisable)")
            AVBLELocalInfo.sharedInstance.saveScopeDevice(self.getBLEManager().activeScopeDevice!)
            
            
            // update name
            self.connectedScopeCompletionMethod()
            
            //get battery level
            AVBLEManager.sharedInstance.readService("Battery")

            
            // check if reconnect occured in pic Firmware update mode
            if self.getFirmwareUpdateService().picNeedsUpdating {
                
                // cancels  cancelfirmware update timer
                self.getFirmwareUpdateService().cancelFirmwareUpdateTimer.invalidate()
                
                // restart update
                switch self.getFirmwareUpdateService().updateType {
                case "Development":
                    self.getFirmwareUpdateService().startPicUpdate(withFilePath: self.getFirmwareUpdateService().devPicFilePath)
                default :
                    self.getFirmwareUpdateService().startPicUpdate(withFilePath: DropBoxFirmwareManager.sharedInstance.picFilePath!)
                }
                
            }
            else if(AVSnowProfileService.sharedInstance.profileTransferDisable){
                print("Don't transfer profiles in tutorial mode")
            }
            else {// in normal mode
                
                //sync any profiles that haven't been synced
                self.getSnowProfileService().getAvailableSnowProfileIdentifiers({ testNums in
                    
                    AVSnowProfileService.sharedInstance.autoGetNewProfiles (testNums) // get all old
                    //AVSnowProfileService.sharedInstance.autoGetNewProfiles ([testNums.last!]) // get only most recent // FOR SINGLE
                })
                
                
                
            }
        })
    }
    
    
    
    /**
     Called when device info is received over BLE.  Waits until all fields of Device ID are filled before calling completion method on get device info
     
     - parameter newData:        new data from BLE
     - parameter characteristic: characteristic which received data
     */
    open func newDeviceInfo(_ newData: Data, characteristic: String) {
        if characteristic == "System ID" {
            // convert data to AVSystemID
            
            var mnfID: UInt = 0 // placeholders TEST
            var orgID: UInt = 2 // placeholders TEST
            if newData.count == 8 {
                (mnfID, orgID) = newData.convertToSystemID()}
         
            let newSystemID = AVSystemId(manufacturerId: mnfID, organizationallyUniqueId: orgID)
            // update SystemID in connectedScopeInfo
            connectedScopeInfo.systemID = newSystemID
        }
        else {
            //convert to string
            let stringData = newData.stringRepresentation()
            print("stringdata =\(stringData) ")
            // update proper field
            switch characteristic {
            case "Hardware Revision":
                connectedScopeInfo.hardwareRevision = stringData
            case "Serial Number":
                connectedScopeInfo.serialNumber = stringData
            case "Model Number":
                connectedScopeInfo.modelNumber = stringData
            case "Manufacturer Name":
                connectedScopeInfo.manufacturerName = stringData
            case "Firmware Version":
                connectedScopeInfo.firmwareRevision = stringData
            default:
                print("unknown characteristic")
            }
        }
        connectedScopeInfoFieldCounter += 1
        print("counter = \(connectedScopeInfoFieldCounter)")
        // If all device info fields have been updated, call completion method
        if connectedScopeInfoFieldCounter == 6 {
            let connectedDevice = AVScopeDevice(deviceInfo: connectedScopeInfo)
            connectedDevice.name = getBLEManager().activePeripheral.name
            deviceInfoCompletionMethod(connectedDevice)
            connectedScopeInfoFieldCounter = 0 // maybe move this to BLEManager.didDisconnect??
        }
        }
    
    
   
    // MARK: Functions to get info/connect
    
    /**
     MEthod to get availible scopes (scopes advertising in area)
     Calls disconnect if a device is connected, and sets intentional disconnect to true.  After a certain timeout period, the completion method is called and the array of found peripherals (scopes) is sent
     
     - parameter timeout:           search timeout (in sec)
     - parameter completionHandler: sends array of CBPeripherals (only scopes)
     */
    open func getAvailableScopeDevices(_ timeout: Double, completionHandler:@escaping (([CBPeripheral]) -> Void)) {
        getAvailibleScopeDevicesCompletionMethod = completionHandler // set completion handler
        getBLEManager().intentionalDisconnect = true
        getBLEManager().availiblePeripherals.removeAll() // clear out availible periphs

        if getBLEManager().isConnected {
        // disconnects from current peripheral - disconnect method will call "startScan"
        getBLEManager().centralManager?.cancelPeripheralConnection(getBLEManager().activePeripheral)
        }
        else {
            getBLEManager().startBLE()
        }
        
        var searchForScopeTimer = Timer()
        searchForScopeTimer = Timer.scheduledTimer(timeInterval: timeout, target: self, selector: #selector(AVScope.endSearch), userInfo: nil, repeats: false)
        
    }
    
    
    /**
     stops searching , and calls completion method
     */
    open func endSearch() {
        print("endSearch")
        getBLEManager().centralManager?.stopScan()
        //call completion method
        getAvailibleScopeDevicesCompletionMethod(getBLEManager().availiblePeripherals)
    }
    
    
  /**
   Connects to a scope
   
   - parameter scopeDevice: scope device to connect to (CB Peripheral - can get peripheral object from "get Availible Scope Devices")
   - throws: ScopeConnectionError if connection failed
   */
    open func connectScopeDevice(_ scopePeripheral: CBPeripheral, completionHandler: ((Void) -> Void)?) {
        
    connectedScopeCompletionMethod = completionHandler!
    getBLEManager().connect(scopePeripheral)
  }
  
  /**
   Get the current connected Scope device.  Reads device info and waits until all device info is updated before calling completion method
   
   - returns: device
   */
    open func getConnectedDevice(_ completionHandler:@escaping (AVScopeDevice)->Void){
    deviceInfoCompletionMethod = completionHandler
    getBLEManager().readService("Device Info")
    
  }
    
    
    /// Gets the current status of the device from BLEmanager
    ///
    /// - returns: AVdevice status of current device status
    open func getCurrentStatus() -> AVDeviceStatus? {
        return (getBLEManager().activeScopeDevice?.currentStatus)
    }
    
    
    // MARK: - Functions to get Services
    
    /**
     Utility Method to get the BLEManager
     
     - returns: A singleton instance to the AVBLEManager
     */
 open func getBLEManager() -> AVBLEManager {
        return AVBLEManager.sharedInstance
    }
    
    /**
     Utility method to get AVBLELocalInfo
     
     - returns: a singleton instance of AVBLELocalInfo
     */
    open func getBLELocalInfo() -> AVBLELocalInfo {
        return AVBLELocalInfo.sharedInstance
    }
    
    
  /**
   Utility method to get the AVSlopeService
   
   - returns: A singleton instance to the AVSlopeService
   */
  open func getSlopeService() -> AVSlopeService {
    return AVSlopeService.sharedInstance
  }
  
  /**
   Utility method to get the AVFirmwareService
   
   - returns: A singleton instance to the AVFirmwareUpdateService
   */
  open func getFirmwareUpdateService() -> AVFirmwareUpdateService {
    return AVFirmwareUpdateService.sharedInstance
  }
  
  /**
   Utility method to get the AVbatteryService
   
   - returns: A singleton instance to the AVBatteryService
   */
  open func getBatteryService() -> AVBatteryService {
    return AVBatteryService.sharedInstance;
  }
  
  
  /**
   Utility method to get the AVSnowProfileService
   
   - returns: A singleton instance to the AVSnowProfileService
   */
  open func getSnowProfileService() -> AVSnowProfileService {
    return AVSnowProfileService.sharedInstance;
  }
  


    //MARK: Functions to register for Notifications
    
    
    /**
     Convenient method to register a specific observer alongside with a selector (function) for Scope slope angle changes
     
     - parameter observer: Observer object
     - parameter selector: Function that will get executed on the observer
     */
    open func registerForSlopeAngleNotification(_ observer: NSObject, selector: Selector) {
        unregisterFromSlopeAngleNotification(observer)
        NotificationCenter.default.addObserver(
            observer,
            selector: selector,
            name: NSNotification.Name(rawValue: AVSlopeAngleNotification),
            object: nil)
        
    }
    
    /**
     Convenient method to unregister a specific observer for Scope slope angle changes
     
     - parameter observer: Observer object
     */
    open func unregisterFromSlopeAngleNotification(_ observer: NSObject) {
        NotificationCenter.default.removeObserver(observer, name: NSNotification.Name(rawValue: AVSlopeAngleNotification), object: nil)
    }
    
  /**
   Convenient method to register a specific observer alongside with a selector (function) for Scope status changes
   
   - parameter observer: Observer object
   - parameter selector: Function that will get executed on the observer
   */
  open func registerForDeviceStatusNotification(_ observer: NSObject, selector: Selector) {
    unregisterFromDeviceStatusNotification(observer)
    NotificationCenter.default.addObserver(
    observer,
    selector: selector,
    name: NSNotification.Name(rawValue: AVStatusDidChangeNotification),
    object: nil)
    
  }
  
  /**
   Convenient method to unregister a specific observer for Scope status changes
   
   - parameter observer: Observer object
   */
  open func unregisterFromDeviceStatusNotification(_ observer: NSObject) {
    NotificationCenter.default.removeObserver(observer, name: NSNotification.Name(rawValue: AVStatusDidChangeNotification), object: nil)
  }
  
  /**
   Convenient method to register a specific observer alongside with a selector (function) for Probing status
   
   - parameter observer: Observer object
   - parameter selector: Function that will get executed on the observer
   */
  open func registerForProbingStatusNotification(_ observer: NSObject, selector: Selector) {
    unregisterFromProbingStatusNotification(observer)
    NotificationCenter.default.addObserver(
    observer,
    selector: selector,
    name: NSNotification.Name(rawValue: AVProbingStatusNotification),
    object: nil)
    
  }
  
  /**
   Convenient method to unregister a specific observer
   
   - parameter observer: Observer object
   */
  open func unregisterFromProbingStatusNotification(_ observer: NSObject) {
    NotificationCenter.default.removeObserver(observer, name: NSNotification.Name(rawValue: AVProbingStatusNotification), object: nil)
  }
  
  /**
   Convenient method to register a specific observer alongside with a selector (function) for snow profiles availability
   
   - parameter observer: Observer object
   - parameter selector: Function that will get executed on the observer
   */
  open func registerForSnowProfilesAvailabilityNotification(_ observer: NSObject, selector: Selector) {
    unregisterFromSnowProfilesAvailabilityNotification(observer)
    NotificationCenter.default.addObserver(
      observer,
      selector: selector,
      name: NSNotification.Name(rawValue: AVSnowProfilesAvailableNotification),
      object: nil)
    
  }
  
  /**
   Convenient method to unregister a specific observer
   
   - parameter observer: Observer object
   */
  open func unregisterFromSnowProfilesAvailabilityNotification(_ observer: NSObject) {
    NotificationCenter.default.removeObserver(observer, name: NSNotification.Name(rawValue: AVSnowProfilesAvailableNotification), object: nil)
  }
    
    
    /**
     Convenient method to register a specific observer alongside with a selector (function) for Battery Level
     
     - parameter observer: Observer object
     - parameter selector: Function that will get executed on the observer
     */
    open func registerForBatteryLevelNotification(_ observer: NSObject, selector: Selector) {
        unregisterFromBatteryLevelNotification(observer)
        NotificationCenter.default.addObserver(
            observer,
            selector: selector,
            name: NSNotification.Name(rawValue: AVBatteryLevelNotification),
            object: nil)
        
    }
    
    /**
     Convenient method to unregister a specific observer
     
     - parameter observer: Observer object
     */
    open func unregisterFromBatteryLevelNotification(_ observer: NSObject) {
        NotificationCenter.default.removeObserver(observer, name: NSNotification.Name(rawValue: AVProbingStatusNotification), object: nil)
    }
    


/**
 Convenient method to register a specific observer alongside with a selector (function) for new name
 
 - parameter observer: Observer object
 - parameter selector: Function that will get executed on the observer
 */
open func registerForNewNameNotification(_ observer: NSObject, selector: Selector) {
    unregisterFromNewNameNotification(observer)
    NotificationCenter.default.addObserver(
        observer,
        selector: selector,
        name: NSNotification.Name(rawValue: AVScopeNamedNotification),
        object: nil)
    
}

/**
 Convenient method to unregister a specific observer
 
 - parameter observer: Observer object
 */
open func unregisterFromNewNameNotification(_ observer: NSObject) {
    NotificationCenter.default.removeObserver(observer, name: NSNotification.Name(rawValue: AVScopeNamedNotification), object: nil)
}
    
    /**
     Convenient method to register a specific observer alongside with a selector (function) for transfer
     - parameter observer: Observer object
     - parameter selector: Function that will get executed on the observer
     */
    open func registerForTransferNotification(_ observer: NSObject, selector: Selector) {
        unregisterFromTransferNotification(observer)
        NotificationCenter.default.addObserver(
            observer,
            selector: selector,
            name: NSNotification.Name(rawValue: AVTransferringNotification),
            object: nil)
        
    }
    
    /**
     Convenient method to unregister a specific observer
     
     - parameter observer: Observer object
     */
    open func unregisterFromTransferNotification(_ observer: NSObject) {
        NotificationCenter.default.removeObserver(observer, name: NSNotification.Name(rawValue: AVTransferringNotification), object: nil)
    }
    
    /**
     Convenient method to register a specific observer alongside with a selector (function) for transfer
     - parameter observer: Observer object
     - parameter selector: Function that will get executed on the observer
     */
    open func registerForScopeFoundNotification(_ observer: NSObject, selector: Selector) {
        unregisterFromScopeFoundNotification(observer)
        NotificationCenter.default.addObserver(
            observer,
            selector: selector,
            name: NSNotification.Name(rawValue: AVScopeFoundNotification),
            object: nil)
        
    }
    
    /**
     Convenient method to unregister a specific observer
     
     - parameter observer: Observer object
     */
    open func unregisterFromScopeFoundNotification(_ observer: NSObject) {
        NotificationCenter.default.removeObserver(observer, name: NSNotification.Name(rawValue: AVScopeFoundNotification), object: nil)
    }
    
    /**
     Convenient method to register a specific observer alongside with a selector (function) for transfer
     - parameter observer: Observer object
     - parameter selector: Function that will get executed on the observer
     */
    open func registerForHighVolumeProfilesNotification(_ observer: NSObject, selector: Selector) {
        unregisterFromHighVolumeProfilesNotification(observer)
        NotificationCenter.default.addObserver(
            observer,
            selector: selector,
            name: NSNotification.Name(rawValue: AVScopeHighVolumeProfilesTransferNotification),
            object: nil)
        
    }
    
    /**
     Convenient method to unregister a specific observer
     
     - parameter observer: Observer object
     */
    open func unregisterFromHighVolumeProfilesNotification(_ observer: NSObject) {
        NotificationCenter.default.removeObserver(observer, name: NSNotification.Name(rawValue: AVScopeHighVolumeProfilesTransferNotification), object: nil)
    }
    
    
    /**
     Convenient method to register a specific observer alongside with a selector (function) for firmware update
     - parameter observer: Observer object
     - parameter selector: Function that will get executed on the observer
     */
    open func registerForFirmwareUpdateAvailibleNotification(_ observer: NSObject, selector: Selector) {
        unregisterFromFirmwareUpdateAvailible(observer)
        NotificationCenter.default.addObserver(
            observer,
            selector: selector,
            name: NSNotification.Name(rawValue: AVScopeFirmwareUpdateAvailible),
            object: nil)
        
    }
    
    /**
     Convenient method to unregister a specific observer for firmware update availible 
     
     - parameter observer: Observer object
     */
    open func unregisterFromFirmwareUpdateAvailible(_ observer: NSObject) {
        NotificationCenter.default.removeObserver(observer, name: NSNotification.Name(rawValue: AVScopeFirmwareUpdateAvailible), object: nil)
    }
    



}



