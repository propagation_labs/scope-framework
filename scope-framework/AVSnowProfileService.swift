//
//  AVSnowProfileService.swift
//  scope-framework
//
//  Created by Olivier Brand on 3/1/16.
//  Copyright © 2016 Olivier Brand. All rights reserved.
//

import UIKit
import CoreLocation
import Crashlytics


// Defines SnowProfile Errors  -- PROBABLY DON'T NEED
public enum SnowProfileError : Error {
    //
    
    /**
     *  deletion error along with the profiles that could not be found
     *
     *  @param  array of snow profile identifiers that could not be found
     *
     *  @return notFound error
     */
    case notFound([String])
    
    /**
     *  deletion error along with the profiles that could not be deleted
     *
     *  @param  array of snow profile identifiers that could not be deleted
     *
     *  @return notDeleted error
     */
    case notDeleted([String])
    
    // Any other critical errors
    /**
     *  Any critical error
     *
     *  @return critical error
     */
    case critical
}


/**
 Defines datatypes to be transferred - used on the profiles to transfer characteristic
  */
public enum dataType: Int {
    case processed = 0
    case shortenedRaw = 2
    case fullRaw = 1
}


/// The AVSnowProfile Class contains all methods and logic relating to receiving profiles, both raw and processed
// The Basic Method for Transferring a Snow Profile is as follows:
// 1. Either read or get notified of profiles on scope from the Profile IDs characteristic,
// 2.  newprofileIds is called with the received BLE data (should be highest test num on device), this is converted into an array of availible test numbers on the device
// 3. AutoGetSnowProfiles is called  with the array of availible profiles (unless the a get profiles method was called - for raw or get old profiles)
// 4. AutoGetSnowProfiles compares the array of availible profiles to the array of saved profiles, and creates an array of untransfferd test numbers to transfer
// 5. Calls "GetProfilesFor" method with array of test numbers to transfer, this sets transfer timers and calls getsnowprofile for first profile in array
// 6."getsnowprofile" writes the profile ID to transfer to scope on the profile to transfer char.  This causes Scope to begin transferring a profile
// 7. Data is received in packets of 20 bytes from Scope. When a packet is received, the "new profiles" method is called and the packets are converted to integers and stored in a buffer until the profile length is reached
// 8. When the profile length is reached,  the data is converted into an AVSnowProfile Object and UI is notified with this profile.  The completion method of getsnowprofile is called and the next profile in the test num to transfer array is transffered via steps 6-8
//9. When all the profiles in test num to transfer have been transferred, the getProfilesFor completion method is called and profile transfer is completed


// There is also an option for the user to stop transfer of profiles after 5 have been transferred (to guard against the device being "frozen" while it transfers all of its profiles.  In this case, a flag will be tripped by the user, causing the recursive loop of getsnowprofile to end. 

//Eventually, test length will be dynamic (so we get 1 pt/mm of depth). this will cause test length to be updated before profiles begin to be transferred

open class AVSnowProfileService: NSObject {
    
    
    //MARK: -  Class properties
    var rawDataEnabled = false // default
    var expectedNumberOfTests = 1 // default but is changed when multiple profiles are asked for
    var testNumsToTransfer = [Int]() // array of test numbers to transfer
    var expectedTestLength = 3080 // default should be  larger than anything possible (also change after profile is successfully received ) // old default was 3080
    var newDataBuffer = [UInt8]() // buffer to hold data as it comes in
    var testsReceived = 0 // buffer to count number of tests received
    
    var rawTestsreceived = [Data]() // holds raw tests when they are received
    let expectedRawDataLength = 60080  // CHANGE!!
    var transferTimer = Timer() // timer for timeout on transfer
    var newProfileNumber = 0 // number of new profiles not viewed yet
    var highVolumeOfProfilesFlag = false {
        didSet {
        print("high vol prof = \(highVolumeOfProfilesFlag )")
        }
        
    }// flag that says we are transferring over 5 profiles (only tripped after 5 transfer)
    var shouldStopHighVolumeFlag = false // flag that says we should not transfer over 5 (user activates) 
    var profileTransferDisable = false //flag to not transfer old profiles (used while in tutorial mode)
    var unsortedProfiles = [AVSnowProfile]() // Array of all profiles on phone (unsorted)
    var sortedProfiles = [[AVSnowProfile]]() // Array of sections of profiles on phone (sorted)
    
    var currentTestNum = 0 // current test number that is being transferred
    var auto_retry_flag = 0 //flag for a single retry if profile timer runs out
    
    
    // MARK: - completion handlers
    fileprivate var profileIDCompletionMethod:(_ profileIds:[Int])  ->Void = {x in x} // completion method for receiveing profile IDS
    fileprivate var didGetIds = false // flag for if profile IDs were received via notify (false) or read (true)
    fileprivate var snowProfileCompletionMethod:(Void)->Void = {x in x} // completion method for loop in getting snowprofiles
    var allSnowProfileCompletionMethod: (([Data]) -> Void)? // completion method called for rawdata
    
    
    
    /** Singleton returning an initialized instance (note that performing this call calls the init() method)
     */
    open static let sharedInstance = AVSnowProfileService()
    
 // MARK: - ERRORs
    
    /**
     Called when the device can not match requested ProfileIDs to ones it has.
     
     - parameter newData: data from Bluetooth (profile Error Characteristic)
     
     - throws: A snowProfileError
     */
    open func newProfileError(_ newData:Data) throws -> Void  {
        let newDataString = newData.stringRepresentation()
        let idArray = newDataString.components(separatedBy: ",")
        throw SnowProfileError.notFound(idArray)
    }
    
    
    
    /// Called when a new profile error is received.  Alert user with the error if it was the most recent, and save the error as a "bad profile"
    ///
    /// - Parameter newData: raw BLE data that was received on the probing error service
    open func newProbingError(newData: Data) {
        let dataInt = newData.convertToInt()[0]
        
        //analytics
        Answers.logCustomEvent(withName: "Probing Error", customAttributes: ["error code":dataInt])
        
        // save profile number as bad profile
        if let currentSerialNum = AVBLEManager.sharedInstance.activeScopeDevice?.info.serialNumber {
            AVCoreDataManager.sharedInstance.saveBadProfile(forSerialNumber: currentSerialNum, forTestNumber: currentTestNum)
        }
        
        if testNumsToTransfer.first! == currentTestNum {
            transferTimer.invalidate()
        }
     
        //
        
        // only send if most recent error
        let mostRecentTestNum = testNumsToTransfer.last!

        if currentTestNum == mostRecentTestNum {
            NotificationCenter.default.post(name: Notification.Name(rawValue: AVProbingStatusNotification),object: self,userInfo: ["errorNum": dataInt])

        }
        //local notification
        newErrorLocalNotify(errorNum: dataInt)
        
        
    /*  // USES ENUM STRUCT TO SEND A SPECIFIC ERROR
        if let probeError = AVProbingErrorCode(status: dataInt) {
            
        
            
            // Notify UI if the error was from the most recent test
            let mostRecentTestNum = testNumsToTransfer.first!
            if currentTestNum == mostRecentTestNum {
                NotificationCenter.default.post(name: Notification.Name(rawValue: AVProbingStatusNotification),object: self,userInfo: ["probing error": probeError])
                print("received error on the most recent test")
            }
            else  {
                print("received error on not the most recent test")
            }
           
          
        }
        else {
            print("no error enum with code \(dataInt), no proper error method was taken")
        }
        */
        
        
        
        // go to get next test (if possible)
        
        //delay until next call - hacky but it works
        let delayInSeconds = 0.5
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
            
            // here code perfomed with delay
            self.testsReceived += 1
            self.snowProfileCompletionMethod()
        }
  
    }
    
    
    //MARK: - New PRofiles 
    
    /**
     Called when new profile ID data is received via BLE.  Checks to see if the GetsnowprofilesIdentifiers method was called, and calls the completion method if it was.  Also, gets location, compares IDs to the ones that were already received, and calls get Snow Profiles on any ids that were not received
     
     - parameter newData: new profile ID data (NSDATA)
     - parameter error:   Error
     */
    open func newProfileIds(_ newData: Data, error: Error?)  {
          testTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(self.testTime), userInfo: nil, repeats: true)
        // convert NSData into last ID (highest test number on device)
        let lastID = newData.convertFromInt16(0, range: 2)[0]
        
        //find minimum test number on device, and make range of test numbers
        let maxNumOfTestsOnDevice = 142 // CHANGE
        var minAvailibleTestNum = 0
        if lastID - maxNumOfTestsOnDevice > 0 {
            minAvailibleTestNum = lastID - maxNumOfTestsOnDevice
        }
        let availibleTestNums = Array(minAvailibleTestNum...lastID)
        print("got last ID  \(lastID)")
        //print("availibleTestNums = \(availibleTestNums)")
        
        if didGetIds {
            profileIDCompletionMethod(availibleTestNums)  // What is the point of this completion method (would we ever want to get just the IDs and not the full profiles??? - YES! for RAW DATA!!
            didGetIds = false
        }
            
        else {
            if AVSnowProfileService.sharedInstance.profileTransferDisable
            {
                autoGetNewProfiles([lastID])
            }
            else
            {
                autoGetNewProfiles(availibleTestNums)
            }
        }
    }
    
    
    /**
     takes availible IDs and compares them to IDs of tests already saved on the device. For any new profiles that the phone does not have, transfer them
     
     - parameter availibleTestNumbers: array of ints of the availible test numbers
     */
    func autoGetNewProfiles(_ availibleTestNumbers: [Int]) {
        testsReceived = 0
        // Start process of getting profiles
        // Get location - and only write if location was not nil
        
        newDataBuffer = [UInt8]()
        
        // reset timer so if a new test is being transferred after a failed one it still is ok (may need to add if let?)
        transferTimer.invalidate()
        
        
        
        // Compare ids to ones already received on the currently connected device! - make array of test numbers to transfer.  Eventually will have to create a separate array of previously transferred test numbers (so deleted ones are not re transferred)
        
        var savedTestNums = [Int]() // array of test numbers saved on device (with the same serial number as connected device)
        let currentProfiles = AVCoreDataManager.sharedInstance.loadProfiles()
        let currentSerialNum = AVBLEManager.sharedInstance.activeScopeDevice?.info.serialNumber
        let shortenedCurrentSerNum = currentSerialNum!.substring(to: currentSerialNum!.characters.index(currentSerialNum!.startIndex, offsetBy: 5))
        for profile in currentProfiles {
            // NEED TO TAKE FIRST 5 CHARACTERS OF STRING TO COMPARE
            let shortenedProfileSerNum = profile.serialNum!.substring(to: profile.serialNum!.characters.index(profile.serialNum!.startIndex, offsetBy: 5))
            
            if shortenedCurrentSerNum == shortenedProfileSerNum {  // only add to array if it is a saved profile with the same serial number -- uncomment when serial num stuff works
                savedTestNums.append(profile.testNum!)
            }
        }
        testNumsToTransfer = [Int]()
        
        //get array of bad profiles (profiles that are errors or corrupted data on scope)
        let badTestNums = AVCoreDataManager.sharedInstance.checkForBadProfiles(forSerNum: currentSerialNum!)
        
        // check which tests have already been transferred
        for testNum in availibleTestNumbers {
            if !savedTestNums.contains(testNum) { // the availble test number is not already saved
                let minTest = AVScope.sharedInstance.getBLEManager().activeScopeDevice?.minTestToTransfer
                
                if minTest! < availibleTestNumbers.last! && testNum < minTest! {
                    //don't add, minimum test number is valid and the test number is less than this minimum test
                }
                else { // test number is greater than a valid minimum test number
                    if !badTestNums.contains(testNum) { // this test number is not in the bad profiles, add it to be transferred
                        testNumsToTransfer.append(testNum)
                    }
                }
            }
        }
        
        
        
        print("testnums to trans = \(testNumsToTransfer)")
        print("bad testnums = \(badTestNums)")
        
        // get the processed profiles!
        if testNumsToTransfer.count != 0 {
            
            // this is a check for test number 0
            if testNumsToTransfer.last != 65535 {
                //send location if availible
                if let location = AVLocationManager.sharedInstance.getLocation() {
                    AVBLEManager.sharedInstance.writeLocation(location.latitude,longitude: location.longitude)
                }
                
                // set expected number of profiles
                expectedNumberOfTests = testNumsToTransfer.count
                
                
                getprofilesfor(testNumsToTransfer.reversed(), typeOfData: .processed, completionHandler:  nil) // transfer all old profiles if there is not a recent
                //getprofilesfor([availibleTestNumbers.last!], typeOfData: .processed, completionHandler:  nil)  // get only most recent //FOR SINGLE
            }
        }
        else {
            print("no new test nums to transfer")
        }
        
    }
    
    
    // test timer stuff - for timing length of profile transfer
    var testTimer = Timer()

    var totalTransferTime = 0.0
    let timeInterval = 0.01
    func testTime() {
        totalTransferTime += timeInterval
        
    }
    
    
    
    /**
     Called when new snow profile data is received.  Store data in buffer and call a completion handler when a full profile is received - this will also convert the data into an array of snowProfiles, save it, and notify UI
     
     - parameter newData: new data from snowprofile service
     - parameter error:   error
     */
    
    open func newProfiles(_ newData: Data, error: Error?) {
        if rawDataEnabled {
            expectedTestLength = expectedRawDataLength
        }
        else {
            //print("processed")
        }
        
        //adds new data to an array of Uint8s
        let receivedDataArray = newData.convertFromInt8(0, range: newData.count)
        //print("received Ints (from Int8) = \(receivedDataArray )")
        for i in receivedDataArray {
            newDataBuffer.append(UInt8(i)) // might need to make convertFromInt 8 only use UINT8s
        }
        
        
        // updates transfer percentage and number of tests received to change a view
        //Call a notification with update percentage
        var transferPercentage = Float(newDataBuffer.count)/Float(expectedTestLength)
        //print("length = \(newDataBuffer.count)")
        NotificationCenter.default.post(name: Notification.Name(rawValue: AVTransferringNotification),object: self,userInfo: ["percent": transferPercentage, "tests": testsReceived, "status": "transferring" ])
        
        
        
        
        // if a full test has been received
        
        if newDataBuffer.count == expectedTestLength {
            
            //test timer
            testTimer.invalidate()
            print("total time = \(totalTransferTime)")
            totalTransferTime = 0.0
            print(" \(testsReceived + 1) of \(expectedNumberOfTests) expected tests received")
            
            print("data recived as Uint8 = \(newDataBuffer)")
            let receivedData = Data(bytes:&newDataBuffer, count: newDataBuffer.count)
            
            
            
            if rawDataEnabled { // raw data specific.  Add new data to array
                rawTestsreceived.append(receivedData)
            }
                
                
                
            else { // processed specific - converts to AVsnowProfile and saves, then notifies UI with profile
                
                let receivedProfile = AVSnowProfile(withBLEData: receivedData) // creates snow profile object with data
                
                // make sure receivedProfile is not a repeat!
                let currentProfiles = AVCoreDataManager.sharedInstance.loadProfiles()
                let currentSerialNum = AVBLEManager.sharedInstance.activeScopeDevice?.info.serialNumber
                let shortenedCurrentSerNum = currentSerialNum!.substring(to: currentSerialNum!.characters.index(currentSerialNum!.startIndex, offsetBy: 5))
                
                print("receivedTestnum = \(receivedProfile.testNum ), received SerNum = \(receivedProfile.serialNum)")
                
                var newFlag = true
                for profile in currentProfiles {
                    if profile.testNum! == receivedProfile.testNum! {
                        let shortenedProfileSerNum = profile.serialNum!.substring(to: profile.serialNum!.characters.index(profile.serialNum!.startIndex, offsetBy: 5))
                        if shortenedProfileSerNum == shortenedCurrentSerNum {
                            
                            newFlag = false
                            
                        }
                    }
                }
                
                // check to make sure there is data
                if (receivedProfile.hardness?.count)! < 1 {
                    newFlag = false
                }
                
                //newFlag = true // ONLY FOR SINGLE
                
                if newFlag { // only proceed if profile is actually new
                    
                    //analytics
                    Answers.logCustomEvent(withName: "Snow Profile Collected", customAttributes: ["profile depth" : receivedProfile.profileDepth!])
                
                    //update location name
                    receivedProfile.locationName = "location name" // default - add in Mnthub reverse geocoding
                    
                     // saves profile
                    AVCoreDataManager.sharedInstance.saveProfile(receivedProfile)
                    
                    // adds profile to savedProfiles property
                    unsortedProfiles.append(receivedProfile)
                    sortProfiles()
                    
                    //updates newProfilenumber
                    updateNewProfileNumber() // could probably just do +1 for more efficiency? 
                    
                    // notifies UI with new Profile
                    NotificationCenter.default.post(name: Notification.Name(rawValue: AVSnowProfilesAvailableNotification),object: self,userInfo: ["profile":receivedProfile])
                    
                    // local notify
                    //newProfilesLocalNotify()// maybe only do if all tests are received
                }
                
            }
            
            
            // change buffers
            testsReceived += 1
            newDataBuffer = [UInt8]()
            expectedTestLength = 3080 // resets in case the profile length is not received initally
            // call cycle completion method
            snowProfileCompletionMethod()
        }
        
        
        
        // if all expected tests have been received
        if testsReceived == expectedNumberOfTests {
            transferPercentage = 0
            
            if rawDataEnabled { // raw data - call completion handler
                allSnowProfileCompletionMethod!(rawTestsreceived)
            }
            else { // processed data - notify UI
                // NSNotificationCenter.defaultCenter().postNotificationName(AVTransferringNotification,object: self,userInfo: ["percent": Int(transferPercentage), "tests": testsReceived, "status": "complete" ])
            }
            
            //calls local notification
            newProfileLocalNotify()
            
            // reset test buffer
            testsReceived = 0
            // stop timer
            transferTimer.invalidate()
        }
        
        
        
    }
    
    
    /**
     completion handler to be called if phone disconnects while transferring raw profiles
     **/
    func transferCompletedRawProfiles() {
        if rawTestsreceived.count > 0 {
            allSnowProfileCompletionMethod!(rawTestsreceived)
        }
        rawTestsreceived = [Data]() //clear completed raw profiles
    }

    
    
    
    /**
     Called when timer expires on transferring profiles.  Post a notification to UI
     */
    func transferTimeout() {
        
        
        // do some notification for transfer error
        let transferPercentage = Float(newDataBuffer.count)/Float(expectedTestLength)
        
        //auto retry
        if auto_retry_flag == 1 {
            getAvailableSnowProfileIdentifiers({ testNums in self.autoGetNewProfiles(testNums)})
            auto_retry_flag = 0;
        }
        
        //NotificationCenter.default.post(name: Notification.Name(rawValue: AVTransferringNotification), object: self, userInfo: ["percent": transferPercentage , "tests": testsReceived, "status": "error" ])
        // reset buffers
        testsReceived = 0
        //newDataBuffer = [UInt8]()
        print("timeout")
        
    }
    
    /**
     Get available identifiers related to available snow profiles
     
     - returns: Array of snow profile identifiers available for download   // Should have the device return ["none"] if there are no snowProfileIds so that completion method is called
     */
    open func getAvailableSnowProfileIdentifiers(_ completionHandler:@escaping ([Int]) -> Void){
        if AVBLEManager.sharedInstance.isConnected
        {
            didGetIds = true
            profileIDCompletionMethod = completionHandler
            AVBLEManager.sharedInstance.readService("Snow Profile")
        }
    }
    
    
    /**
     Gets a snow profile from a scope device
     
     - parameter typeOfData:        datatype to get (see dataType Enum)
     - parameter profileNumber:     profile number to get
     - parameter completionHandler: completion handler to be called
     */
    open func getSnowProfile(_ typeOfData: dataType, profileNumber: Int ,completionHandler:@escaping (Void) -> Void) {
        snowProfileCompletionMethod = completionHandler
        AVBLEManager.sharedInstance.writeProfileID(profileNumber, dataType: typeOfData.rawValue)
        
    }
    
 
    /**
     Method to recursively  call completion handler of get snowProfile.  Also has logic to notify UI if more than 5 profiles have transferred at once and respond to user input

     
     - parameter testNums:          array of test numbers to get profiles for
     - parameter typeOfData:        type of data to get (use enum)
     - parameter completionHandler: completion handler for raw data (optional)
     */
    func getprofilesfor(_ testNums : [Int], typeOfData: dataType, completionHandler:(([Data]) -> Void)? ) {
        
        
        allSnowProfileCompletionMethod = completionHandler
        
        
        //setup timer if it is the first call
        if testsReceived == 0 {
            //  reset high volume flags
            highVolumeOfProfilesFlag = false
            shouldStopHighVolumeFlag = false
            
            var allowedTimePerTest = 5.0  // time per processed test
            if rawDataEnabled {
                allowedTimePerTest = 30.0 //time per raw test
            }
            //start timer
            transferTimer = Timer.scheduledTimer(timeInterval: allowedTimePerTest * Double(expectedNumberOfTests), target: self, selector: #selector(self.transferTimeout), userInfo: nil, repeats: false)
            auto_retry_flag = 1 //set the retry flag when the timer gets started (its turned off when we retry)
            
        }
        
        //keep calling if not all tests have been received
        if testsReceived < testNums.count {
            if testsReceived  == 5 {
                // ask user for confirmation after the 5th test has transferred (if there were that many), if the user doesn't respond, just auto transfer everything (user input would stop transfer)
                //send notification, and set flag (to be read by main VC to show alert if main VC is not active )
                highVolumeOfProfilesFlag = true
                
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: AVScopeHighVolumeProfilesTransferNotification),object: self,userInfo: ["totalProfiles": expectedNumberOfTests])
                
                
            }
            if shouldStopHighVolumeFlag {  // user has intentionally tripped transfer flag, set the minimumTestNum to transfer so they don't get prompted again on next transfer attempt
                let minimumTestNumberToTransfer = testNums[0] // based on test nums being sorted from newest to oldest
                
                // set and save minTestToTransfer
                let activeScope = AVScope.sharedInstance.getBLEManager().activeScopeDevice!
                if minimumTestNumberToTransfer < 60000 { // check for negative test numbers
                activeScope.minTestToTransfer = minimumTestNumberToTransfer
                AVBLELocalInfo.sharedInstance.saveScopeDevice(activeScope)
                }
                
            }
            else {   //keep transferring as user has not tripped flag to stop high volume transfers
                // set current testnum
                currentTestNum = testNums[testsReceived]
                getSnowProfile(typeOfData, profileNumber: currentTestNum, completionHandler: {Void in self.getprofilesfor(testNums, typeOfData: typeOfData, completionHandler: completionHandler) } )
            }
        }
        
    }
    
    

    /**
     Enables or disables the retrieval of raw data
     
     - parameter enableRawData: true to enable, false otherwise. The default is false
     */
    open func enableRawData(_ enableRawDatas: Bool) {
        rawDataEnabled = enableRawDatas;
    }
    
    
    
    /**
     updates expectedTestLength with the data received via BLE as Int16.  Adds in header data length and rounds up to nearest 20 (packets are in length of 20)
     
     - parameter newData: 
     */
    func updateExpectedTestLength(_ newData : Data) {
        
        let newLength = newData.convertFromInt16(0, range: 2)[0]
        let (_,expectedHeaderLength) = getIndiciesAndLength() // don't need to do this every time - could change to calc once to increase speed??
        let totalExpectedData = expectedHeaderLength + newLength
        var remainder = 0
        if (totalExpectedData % 20) != 0
        {
            remainder = 20 - (totalExpectedData % 20)
        }

        
        
        print("recevied test length = \(newLength)")
        //check to make sure the expected test length is reasonable (under 3 m)
        if totalExpectedData < 3000 {
            expectedTestLength = totalExpectedData + remainder
        }
        else {
            // scope is sending unreasonable test length, save this test number as one never to transfer, and alert user to restart scope (timeout will cause 
            print("test length received is unreasonable")
            
            
            // save this test number + ser num as one not to trans 
            let serNum = AVBLEManager.sharedInstance.activeScopeDevice?.info.serialNumber
            AVCoreDataManager.sharedInstance.saveBadProfile(forSerialNumber: serNum!, forTestNumber: currentTestNum)
            
            transferTimer.invalidate()  // disable timeout because it will occur and we should not hit user with two notifications
            // notify user
            NotificationCenter.default.post(name: Notification.Name(rawValue: AVTransferringNotification), object: self, userInfo: ["percent": 1.0 , "tests": testsReceived, "status": "lengthError" ])
            // reset buffers
            testsReceived = 0
        }
        
        print("newlength = \(newLength ), totalExpectedData = \(totalExpectedData), remainder = \(remainder), expectedTestLength = \(expectedTestLength) ")
        
        
        
        // maybe add a check in here to see if there is data waiting in the buffer for profile (would happen if profile length was received after the entire profile)
    }
    
    
    
    //MARK: Header DAta
    
    // Array of header types and lengths.  THIS AND THE SWITCH STATEMENT (in AVSnowProfile) ARE THE ONLY THINGs THAT NEEDS TO BE CHANGED IF HEADER CHANGES!
    let order = [["temp","latitude","longitude", "testNum", "profileDepth", "battery", "test time", "error code","accel","gyro","force","optic","serial","PIC","ARM","mainPCB","NRFPCB"],
                 [ 1   , 4        ,   4       , 2         , 2              ,1        , 4          , 1            ,1     , 2    , 10    , 2     , 8     ,  8   , 8   ,1        , 1       ]]
    
    
    
    /// Calculates the indicies and the total length of the header data structure, based on the order variable
    ///
    /// - Returns: Array of starting indicies for each header data type, and total dataLength of header data (32 bit aligned)
    func getIndiciesAndLength() -> ([Int],dataLength: Int) {
        
        var indicies = [0] // starting indicies of each header data type - aligned for 32 bit
        var previousIndex = 0
        for i in 0..<order[1].count {
            let currentLength = order[1][i] as! Int
            var indexToAdd = currentLength + previousIndex
            let next = i+1
            
            
            if next < order[1].count  { // make sure we are not at end
                let nextLength = order[1][i+1] as! Int
                
                switch indexToAdd % 4  {
                case 0: // no buffer needed (already 32 bit aligned)
                    break
                case 1:
                    if nextLength < 4 {
                        if nextLength == 2 {
                            // add buffer of 1 to align to 16 bit
                            indexToAdd += 1
                        }
                        // otherwise, do nothing (1 or 3 byte length will auto realign to 16 or 32)
                    }
                    else { // realign to 32 bit
                        indexToAdd += 3
                    }
                    
                case 2:
                    if nextLength > 3 {
                        indexToAdd += 2 // realign to 32 bit
                    }
                    // otherwise, do nothing (1 or 2 byte will auto realign to 32 (1 will be addressed in next loop)
                    
                case 3:
                    if nextLength != 1 {
                        indexToAdd += 1 //realign to 32 bit
                    }
                // otherwise, do nothing (1 byte will auto realign to 32)
                default:
                    break
                }
                
                indicies.append(indexToAdd)
                previousIndex = indexToAdd
                
            }
        }
        
        print("indicies = \(indicies)")
        
        // get total length
        let headerLength = indicies.last! + 1
        let remainder = headerLength % 4
        var alignedLength = headerLength
        if remainder != 0 {
            alignedLength = headerLength + 4 - remainder
        }
        
        return (indicies,alignedLength)
    }
    //MARK: Hardness conversions
    
    
    /// Converts hand hardness to kPa.  THIS FUNCTION IS DISCONTINUOS BUT MATCHES FUNCTION IN FW.  Should be changed to 1:1 linear scale at low end?   This function converts the 0-255 sent from the device back into meaningful kPa values.  It should not be changed without changing firmware
    ///
    /// - Parameter hardnessArray: array of HH values 0-255
    /// - Returns: array of corresponding kPa values
    
    func HH51_to_kPa(hardnessArray:[Int] ) -> [Float]{
        // print("in : \(hardnessArray)")
        var kPa_hardnesses = [Float]()
        for cur_hh in hardnessArray{
            if cur_hh > 38 {
                let cur_kpa = Double(pow(((Double(cur_hh)/51.0)/0.320), 2.4))
                kPa_hardnesses.append(Float(cur_kpa))
            }
            else {
                
                
                kPa_hardnesses.append(Float(Double(cur_hh) * 16.0/38.0))
            }
            
        }
        //print("out : \(kPa_hardnesses)")
        return kPa_hardnesses
    }
    
    /// Converts kPa to hand hardness, linear scale up until intersection with log scale to preserve hardness values at low end.  This function changes how kPa values are plotted on graph and can be altered independantly of firmware
    ///
    /// - Parameter hardnessArray: array of kPa values (float)
    /// - Returns: array of scaled HH values (0-255)
    
    func kPa_to_HH51(hardnessArray:[Float] ) -> [Int]{
        //print("in2 : \(hardnessArray)")
        var HH51_hardnesses = [Int]()
        for cur_kpa in hardnessArray{
            
            if cur_kpa < 28 { //28 is real intersection
                HH51_hardnesses.append(Int(cur_kpa*38.0/16.0))
                
                
            }
            else {
                let cur_hh = 0.320*pow(Double(cur_kpa), 1/2.4)*51
                HH51_hardnesses.append(Int(ceil(cur_hh)))
            }
        }
        // print("out : \(HH51_hardnesses)")
        
        return HH51_hardnesses
    }

    
    /*
     * round to near HH interval include plus or minus vals
     */
    func HH_round(val : Int) -> Int {
        let tick_interval = 17 //there are 17 points on the graph for each third hardness step. used for snapping to a tick mark.
        return Int(round(Double(val) / Double(tick_interval))) * tick_interval
    }
    
    /*
     * TODO: figure out a way so this does not need to be run every time the view setting is switched
     */
    func blockify_profile(hardnessArray:[Int]) -> [Int] {

        let window_interval = 5
        let hardness_thresh = 25 //technically 17 is one step change in hardness. TODO: tune this
        var blocky_hardness = [Int]()
        var block_bounds = [0]
        
        if hardnessArray.count < window_interval
        {
            blocky_hardness = hardnessArray
            return blocky_hardness
        }
        
        /** find difference boundaries **/
        var  i = 1
        while i < hardnessArray.count - window_interval - 1
        {
            var hardness_diff = 0
            var window_size = window_interval
            while(hardness_diff <  hardness_thresh && i+window_size < hardnessArray.count)
            {
                hardness_diff = abs(hardnessArray[i+window_size] - hardnessArray[i])
                window_size += window_interval
            }
            if(i+window_size < hardnessArray.count)
            {
                block_bounds.append(i+window_size - window_interval)
            }
            else
            {
                block_bounds.append(hardnessArray.count)
            }
            i+=(window_size-window_interval)
        }
        //block_bounds.append(hardnessArray.count)
        
        //print("creating blocks from boundaries \n")
        var prev_bb_ind = block_bounds[0]
        for bb_ind in block_bounds[1..<block_bounds.count]{
            let current_block = hardnessArray[prev_bb_ind..<bb_ind]
            //print("bb_ind = \(bb_ind) prev_bb_ind = \(prev_bb_ind) ")
            //print("size of block_bound = \(block_bounds.count), hh array  = \(hardnessArray.count)")
            let mean_val = current_block.reduce(0, +)/(bb_ind - prev_bb_ind)
            
            for _ in prev_bb_ind..<bb_ind
            {
                blocky_hardness.append(mean_val)
            }
            prev_bb_ind = bb_ind
        }
        return blocky_hardness
        
    }
    
    
    
    //MARK: - savedProfileFunctions
    
    /// Resorts the unsorted profiles and updates the sorted profiles  - uses the splitProfilesIntoSections in the Location Manager
    ///
    /// - parameter profileToAdd: AVsnowProfile
    func sortProfiles() {
        // sorts array
        sortedProfiles = AVLocationManager.sharedInstance.splitProfilesIntoSections(unsortedProfiles) // could probably do this in a more efficient way
    }
    
    
    
    /**
     checks how many profiles are unread. Also checks if new profiles have  Updates the newProfileNumber Property
     */
    func updateNewProfileNumber() {
        var counter = 0
        
        for profile in unsortedProfiles {
            
            
            
            if profile.isNew! { // profile is saved as new
                // check and resave profiles
                if profile.checkNewTime() { // profile also has not expired
                    counter += 1
                }
                    
                else { // profile expired
                    // resave profile with false isNew
                    let oldProfData = AVCoreDataManager.sharedInstance.convertProfileToJSONData(profile)
                    profile.isNew = false
                    AVCoreDataManager.sharedInstance.updateProfile(oldProfData!, updatedProfile: profile)
                }
            }
        }
        newProfileNumber = counter
    }
    
    //MARK: - Local Notifications
    
    
    /// Schedules a local notification with a given message
    ///
    /// - parameter sender: self
    func scheduleLocalNotify(alertTitle: String, alertAction: String) {
        let settings = UIApplication.shared.currentUserNotificationSettings
        
        if settings!.types == UIUserNotificationType() {
          print("can't schedule local notify because I don't have permission")
        }
        let notification = UILocalNotification()
        notification.fireDate = Date(timeIntervalSinceNow: 0)
        notification.alertBody = alertTitle
        notification.alertAction = alertAction
        notification.applicationIconBadgeNumber = newProfileNumber
        notification.soundName = UILocalNotificationDefaultSoundName
        UIApplication.shared.scheduleLocalNotification(notification)
    }
 
    // Calls a local notification for new profiles
    func newProfileLocalNotify() {
        scheduleLocalNotify(alertTitle: "New Scope Snow Profile received!", alertAction: "View Profile")
    }
    
    // calls a local notification for a profile error
    func newErrorLocalNotify(errorNum: Int) {
        // ADD code to convert INT to probing error enum/string
        scheduleLocalNotify(alertTitle: "Probing Error #" + String(errorNum), alertAction: "Please Probe Again")
    }
    
}
