//
//  AVBLEInfo.swift
//  scope-framework
//
//  Created by Garrett Harmsen on 3/15/16.
//  Copyright © 2016 Olivier Brand. All rights reserved.
//
// The AVBLEInfo class holds the info required for each peripheral, service, and characteristic for the BLE communication

import Foundation
import CoreBluetooth

class AVBLEInfo {
    // Singleton
    static let sharedInstance = AVBLEInfo()
    
    
    // MARK: Peripheral info
    // this is currently not being used for anything, but it could be?
    
    // Peripheral UUID
    let sherburneUUID = UUID(uuidString: "8007CF0E-EFEE-69CA-2F2C-6FB417FD5F7A") // put in real UUID for Scope - NEED to get unique for each??
    
    //advertisement data
    let advertisementData = ["key":"value"] // put real advertisement data in here
    
    //Peripheral Name
    let name = "sherburne"  // put in real name for Scope (User generated? Serial number??)
    
    
    // MARK: - Service info
    
    
    // Service UUIDS 
    // these are shortened UUIDs and may need to change for BLE SIG compliance
    var serviceUUID:[String:CBUUID] {
        return convertToUUID(["Device Status": "0x119B" ,"Battery": "0x180F" ,"Device Info": "0x180A" ,"Probing Errors": "0x123A"  ,"Snow Profile": "0x145C" ,"Slope": "0x112C", "Custom Info":"0x761D", "Firmware" : "0x1123"])
    }
    
    
    // MARK: - characteristic Info
    
    // Device Status
    var DeviceStatusCharUUID:[String:CBUUID] { return convertToUUID(["Status Type":"0x2222"])}
    let DeviceStatusCharNotify = ["Status Type":true]
    //Battery
    var BatteryCharUUID:[String:CBUUID] {return convertToUUID(["Battery Level": "0x2A19"])}
    let BatteryCharNotify = ["Battery Level": true]
    //Device Info
    var DeviceInfoCharUUID:[String:CBUUID] { return convertToUUID(["Hardware Revision": "0x2A27", "Serial Number":"0x2A25", "Model Number":"0x2A24", "Manufacturer Name":"0x2A29", "Firmware Version": "0x2A26", "System ID":"0x2A23"])}
    let DeviceInfoCharNotify = ["Hardware Revision": false, "Serial Number":false, "Model Number":false, "Manufacturer Name":false, "Firmware Version":false, "System ID":false]
    //Probing Errors
    var ProbingErrorsCharUUID:[String:CBUUID] {return convertToUUID(["Error Type":"0x1234"])}
    let ProbingErrorsCharNotify = ["Error Type":true]
    //Snow Profile
    var SnowProfileCharUUID:[String:CBUUID] {return convertToUUID(["Profiles":"0x321A", "Profiles ID":"0x321B", "Profiles to transfer":"0x321C", "Profile Error":"0x321E","Location":"0x321D","Profile Length":"0x321F","Raw Data":"0x3219"])}
    let SnowProfileCharNotify = ["Profiles":true, "Profiles ID":true, "Profiles to transfer":false, "Profile Error":true,"Location":false,"Profile Length":true,"Raw Data":true]
    // Slope
    var SlopeCharUUID:[String:CBUUID] {return convertToUUID(["Slope Angle":"0x4322"])}
    let SlopeCharNotify = ["Slope Angle":true]
    // Custom Info
    var CustomCharUUID:[String:CBUUID] {return convertToUUID(["Name":"0x792F"])}
    let CustomCharNotify = ["Name":false]
    // Firmware 
    var FirmwareCharUUID: [String:CBUUID] { return convertToUUID(["Data":"0x5813","Command":"0x5812"])}
    let FirmwareCharNotify = ["Data": false, "Command": true]

    
    
    /**
     converts UUID dictionaries of strings/Ints to dictionaries of strings/CBUUIDs
     
     - parameter IntDict: dictionary of form [String:Int]
     
     - returns: dictionary of form [String: CBUUID]
     */
    func convertToUUID(_ IntDict :[String:String])-> [String:CBUUID] {
        
        let baseBeginning = "0x0000"
        let baseEnd = "1212EFDE1523785FEF13D123"
        let keys = Array(IntDict.keys)
        var UUIDDict = [String:CBUUID]()
        for i in keys {
            
            let shortUUID = IntDict[i]!
            /*
            //drop 0x
            shortUUID.remove(at: shortUUID.startIndex)
            shortUUID.remove(at: shortUUID.startIndex)
            // append BASE UUID
            let fullUUIDString = baseBeginning + shortUUID + baseEnd
 */
            //add to dictionary
            UUIDDict[i] = CBUUID(string: shortUUID)
        }
        return UUIDDict
    }
}

