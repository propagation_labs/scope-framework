//
//  AVBatteryService.swift
//  scope-framework
//
//  Created by Olivier Brand on 3/1/16.
//  Copyright © 2016 Olivier Brand. All rights reserved.
//

import UIKit


/// The battery service defines the necessary functions to request the battery level
/// from a Scope device, and logic to interpret the BLE data as a battery level
open class AVBatteryService: NSObject {
  
  /** Singleton
   */
  open static let sharedInstance = AVBatteryService()
    
   
    //completion handler
    fileprivate var batteryCompletionMethod:(_ batteryLevel:Int)->Void = {x in x}
    fileprivate var didGetBatteryLevel = false // bool to determine if get battery level was called
    
   
    
    /**
     Called when new battery info is received over BLE.  Converts to a Int between 0-100 and calls notification or completion method if applicable
     
     - parameter newData: bettery level data
     - parameter error:
     */
  open func batteryUpdated(_ newData: Data, error: Error?) {
        let newBattery = newData.convertToInt()[0]
    
    print("received battery update to \(newBattery)")
    
        if newBattery <= 100 && newBattery >= 0 {
              AVBLEManager.sharedInstance.activeScopeDevice!.batteryLevel = newBattery
            
            if didGetBatteryLevel  {
                batteryCompletionMethod(newBattery)
                didGetBatteryLevel = false
            }
            else {
            NotificationCenter.default.post(name: Notification.Name(rawValue: AVBatteryLevelNotification),object: self, userInfo: ["battery level":newBattery])
            }
          
        }
        else {
            print("invalid battery level received. LEvel = \(newBattery)")
        }
    }
   
    
    /**
     Calls read on battery level characteristic.  the DidGetBatteryLevel variable is set to true so that the completion handler is called
     
     - parameter completionHandler: closure with logic to deal with results of battery level.
     */
    open func getBatteryLevel(_ completionHandler:@escaping (_ batteryLevel:Int)->Void) {
    didGetBatteryLevel = true
    batteryCompletionMethod = completionHandler
    AVBLEManager.sharedInstance.readService("Battery")

  }

}
