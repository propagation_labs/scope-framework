//
//  AVScopeDevice.swift
//  scope-framework
//
//  Created by Olivier Brand on 3/8/16.
//  Copyright © 2016 Olivier Brand. All rights reserved.
//
// Class to represent a scope device.  Holds all device specific information
import UIKit

open class AVScopeDevice: NSObject {
  open var info : AVDeviceInfo // device info
  open var name: String? // name of scope (changeable by user)
  open var active: Bool // if the scope is currently active (either connected or saved as the scope to auto connect to)
  open var currentStatus : AVDeviceStatus? //status of the device
  open var batteryLevel : Int? // current battery level of device
  open var minTestToTransfer: Int? // minimum test to transfer from this device (set if user does not want to transfer all old tests)
    
  var tempName: String? //name to update to
    
    
  /**
   Builds a AVScopeDevice (should be used when loaded from BLE)
   
   - parameter deviceInfo: information related to the found device
   
   - returns: instance of a scope device
   */
  public init(deviceInfo: AVDeviceInfo) {
    self.info = deviceInfo
    self.active = true
    self.minTestToTransfer = AVBLELocalInfo.sharedInstance.getMinimumTestNumToTrans(forScopeSN: deviceInfo.serialNumber)
    
  }
    
    /**
     Builds an AVScopeDevice (should be used when loaded from data)
     
     - parameter deviceInfo: info related to the device
     - parameter name:       name of device (could be changed by user)
     - parameter active:     is this device the active one?
     
     - returns: instance of a scope device
     */
    public init(deviceInfo: AVDeviceInfo, name: String, active: Bool) {
        self.info = deviceInfo
       // self.info.serialNumber = "00121" // TEST
        self.name = name
        self.active = active
        self.minTestToTransfer = AVBLELocalInfo.sharedInstance.getMinimumTestNumToTrans(forScopeSN: deviceInfo.serialNumber)

    }
    
  
    /**
     Changes the device name to a string + the last 4 digits of the serial number
     
     - parameter newName: new string name (not including serial number)
     */
    open func changeName(_ newName: String) {
        let serNum = info.serialNumber
        let index = serNum.characters.index(serNum.endIndex, offsetBy: -4)
        let serialNumDigits = serNum.substring(from: index)
        
        tempName = newName + serialNumDigits
        
        // sends new name over BLE
        AVBLEManager.sharedInstance.writeName(tempName!)
    }
    
    /**
     Called when write method of changing name is confirmed.  Tells local to change name, and saves that info
     */
    func confirmedNameChange() {
        name = tempName! //sets tempname as the actual name
        AVBLELocalInfo.sharedInstance.saveScopeDevice(self)
        NotificationCenter.default.post(name: Notification.Name(rawValue: AVScopeNamedNotification),object: self, userInfo: nil)
    }
    
    /**
     Called when new name data is received from BLE.  updates the activeScopeDevice.name  ???what is this? 
     
     - parameter data: data containing name as String
     */
    func updateNameFromBLE(_ data: Data) {
        let stringData = data.stringRepresentation()
        name = stringData
        NotificationCenter.default.post(name: Notification.Name(rawValue: AVScopeNamedNotification),object: self, userInfo: nil)

    }
    
  
  /**
   Get the deviceInfo related to the Scope Device
   
   - returns: device info
   */
  open func getDeviceInfo() -> AVDeviceInfo {
    return self.info
  }
  
}
