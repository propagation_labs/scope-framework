//
//  AVBLEManager.swift
//  scope-framework
//
//  Created by Garrett Harmsen on 3/9/16.
//  Copyright © 2016 Olivier Brand. All rights reserved.
//

// Handles all  bluetooth events - calls functions in other classes based on BLE events



import Foundation
import CoreBluetooth
import UIKit




/// The AVBLEManager Class handles all BLE events
open class AVBLEManager: UIViewController, CBCentralManagerDelegate,CBPeripheralDelegate {
    // Singleton
    static let sharedInstance = AVBLEManager()
    
    //MARK: - BLE Properties
    var centralManager : CBCentralManager?
    var availiblePeripherals = [CBPeripheral]() // peripherals that have been discovered
    var activePeripheral: CBPeripheral! // currently active peripheral (if not connected, it is the previously connected peripheral)
    var activeScopeDevice: AVScopeDevice? //currently active scope (not necessarily connected, but the scope that should be auto connected to if it's not connected)
    var intentionalDisconnect = false //flag for if a disconnect was intentionally called (ex: from the user trying to connect to a new scope), this flag is used to disable auto reconnect
    var DFUDisconnect = false // flag for if a disconnect was intentionally caused by switching to DFU mode (for firmware update)
    var isConnected = false // is a scope connected
    //var newDataFlag = false
    var discoveredServicesCounter = 0 // counter to show how many services have been discovered. Used to make sure all services have been discovered before calling connection completion methods
    var connectTimer = Timer()
    
    
    //MARK: - Characteristics
    // characteristic properties used to read/write to characteristics.  These are all set when characteristics are discovered 
    
    var statusTypeCharacteristic:CBCharacteristic?
    
    var batteryLevelCharacteristic:CBCharacteristic?
    
    var hardwareRevisionCharacteristic:CBCharacteristic?
    var serialNumberCharacteristic:CBCharacteristic?
    var modelNumberCharacteristic:CBCharacteristic?
    var manufacturerNameCharacteristic:CBCharacteristic?
    var firmwareVersionCharacteristic:CBCharacteristic?
    var SystemIDCharacteristic:CBCharacteristic?
    
    var errorTypeCharacteristic:CBCharacteristic?
    
    var profileCharacteristic:CBCharacteristic?
    var profileIDCharacteristc:CBCharacteristic?
    var profilesToTransferCharacteristic:CBCharacteristic?
    var profilesToDeleteCharacteristic:CBCharacteristic?
    var locationCharacteristic:CBCharacteristic?
    var profileLengthCharacteristic: CBCharacteristic?
    var rawDataCharacteristic: CBCharacteristic?
    
    var slopeAngleCharacteristic: CBCharacteristic?
    
    var nameCharacteristic: CBCharacteristic?
    
    var picUpdateCharacteristic: CBCharacteristic?
    var firmwareDataCharacteristic: CBCharacteristic?
    var firmwareCommandCharacteristic: CBCharacteristic?
    
    // Notification names
    open let AVBLEErrorNotification = "com.avatech.scope.BLEErrorNotification"
    
    
    
    //MARK: -  BLE Methods
    
    /**
     initializes BLE objects and starts scanning for Peripherals.  Also sets the active scope if there is a saved active scope
     */
    func startBLE() {
        centralManager = nil
        centralManager = CBCentralManager(delegate:self, queue: nil) // maybe change queue?
    
        if let savedScope = AVBLELocalInfo.sharedInstance.loadActiveScope() {
            activeScopeDevice = savedScope
            
        }
    }
    
    /**
     Begins the search for devices, scans for array of services in BLE INFO.  Also could scan for all peripherals (only used when in DFU mode)
     */
    func startScan(onlyForScopes: Bool) {
        let servicesToScan = Array(AVBLEInfo.sharedInstance.serviceUUID.values) // looks for all services
        let singleService = [AVBLEInfo.sharedInstance.serviceUUID["Snow Profile"]!] // only looks for device status
        if onlyForScopes {
        //print("services to scan = \(servicesToScan)")
        centralManager?.scanForPeripherals(withServices: singleService, options: nil)
        }
        else {
            print("scanning for any peripheral")
            centralManager?.scanForPeripherals(withServices: nil, options: nil)
        }
    }
    
    
    
    
    /**
     Connects to a peripheral.  Stops lookings for others, and sets active peripheral
     
     - parameter peripheral: CBPeripheral object to connect to
     */
    func connect(_ peripheral: CBPeripheral) {
    
        self.centralManager?.stopScan()
        self.activePeripheral = peripheral
        self.activePeripheral.delegate = self
        self.centralManager?.connect(activePeripheral, options:nil)
        print("connecting to Scope")
        
        
        // retry connection if timeout occurs
        connectTimer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.connectionTimeout), userInfo: nil, repeats: false)
        
    }
    
    /// CAlled when connect timer ends.  Retries connection
    func connectionTimeout() {
        activePeripheral = nil
        print("set periph to nil")
        startScan(onlyForScopes: false)
       //connect(activePeripheral)

    }
    
    //MARK: Read Methods
    /**
     Calls read on relevant characteristics of a specific service
     
     - parameter service: service to be read
     */
    func readService(_ service: String) {
        
        if isConnected {
            var characteristicToRead:[CBCharacteristic?] = []
            switch service{
            case "Battery":
                characteristicToRead = [batteryLevelCharacteristic]
            case "Device Info":
                characteristicToRead = [hardwareRevisionCharacteristic,serialNumberCharacteristic,modelNumberCharacteristic,manufacturerNameCharacteristic,firmwareVersionCharacteristic,SystemIDCharacteristic]
            case "Snow Profile":
                characteristicToRead = [profileIDCharacteristc]
            case "Device Status":
                characteristicToRead = [statusTypeCharacteristic]
            // Add in other services
            default:
                print ("invalid service to read")
            }
            for char in characteristicToRead {
               // print("char = \(char)")
                activePeripheral.readValue(for: char!)
            }
        }
            
        else {
            print( "cannot read because no scope is connected")
        }
    }
    
    //MARK: Write Methods
    
    /*
    // Writes a device status to status type characteristic
    func writeStatus(_ status: AVDeviceStatus) {
        var intVar  = status.rawValue
        let data = Data(bytes: &intVar, count: MemoryLayout<Int>.size)
        writeRawData(data,characteristic: "Status Type")
    }
    */
    
    
    /**
     Writes a location to the location characteristic.  Writes as 2x Float 32
     
     - parameter latitude:  Float
     - parameter longitude: Float
     */
    func writeLocation(_ latitude:Float, longitude:Float) {
        var location: [Float32] = [Float32(latitude),Float32(longitude)]
        let data = Data(bytes: &location, count: 2*MemoryLayout<Float32>.size)
            print("writing for location, data = \(data), size = \(data.count)")
 
        writeRawData(data, characteristic: "Location")
    }
    
    
    
    /**
     Writes a new name to the name characteristic
     
     - parameter nameString: new name (includes custom name + last 4 digits of serial num)
     */
    func writeName(_ nameString: String) {
        let newString = nameString as NSString
        let data = Data(bytes: newString.utf8String!, count: newString.length)
        print("tried to write name")
        writeRawData(data, characteristic: "Name")
    }
    
    /** 
     Writes firmware instruction code to the firmware instruction characteristic 
     
     - parameter firmwareInstruction: firmware instruction enum to write
 */
    func writeFirmwareInstruction(_ instruction: firmwareInstruction)  {
        var intVar = instruction.rawValue
        let data = Data(bytes: &intVar, count: 1)

        writeRawData(data, characteristic: "Firmware Command")
        
    }
    
    
    /**
     Writes profile ID and datatype to profiles to transfer.  This is called to ask for profiles from the scope
     
     - parameter ID: test number to transfer
     - parameter dataType: data type to transfer
     */
    func writeProfileID(_ ID:Int, dataType: Int) {
        var IDVar = UInt16(ID)
       // print("ID var = \(IDVar)")
        var dataTypeVar = UInt16(dataType)
        let data = NSMutableData(bytes: &dataTypeVar, length: 2)
        data.append(Data(bytes: &IDVar, count: 2))
        print("writing profile ID \(ID), bytes = \(data)")
        
        writeRawData(data as Data, characteristic: "Profiles to Transfer")
    }
    
    
    
    
    /**
     Sends Raw data over BLE to a specific characteristic.  This is the underlying write method called by all the other write methods.  Will always use .writewithresponse unless it is writing firmware data (for speed)
     
     - parameter data:           raw data to be sent (NSData)
     - parameter characteristic: characteristic to be updated (String - refer to AVBLEInfo for list of strings)
     */
    
    func writeRawData(_ data:Data, characteristic: String) {
        var destination:CBCharacteristic?
        
        var writeType = CBCharacteristicWriteType.withResponse
        //Send data to peripheral
        switch characteristic {
        case "Name":
            destination = nameCharacteristic!
        case "Location":
            destination = locationCharacteristic!
        case "Status Type":
            destination = statusTypeCharacteristic!
        case "Profiles to Transfer":
            destination = profilesToTransferCharacteristic!
        case "Profiles to Delete":
            destination = profilesToDeleteCharacteristic!
        case "Firmware Command":
            destination = firmwareCommandCharacteristic!
        case "Firmware Data":
            destination = firmwareDataCharacteristic!
            writeType = .withoutResponse
        default:
            print("characteristic for write is not valid")
        }
        
        // will confirm that write is successful
        
        
        //send data in lengths of <= 20 bytes
        let dataLength = data.count
        let limit = 20
        
        print("writing to char UUID \(destination!.uuid)")
        //Below limit, send as-is
        if dataLength <= limit {
            activePeripheral.writeValue(data, for: destination!, type: writeType)
        }
            
            //Above limit, send in lengths <= 20 bytes
        else {
            
            var len = limit
            var loc = 0
            var idx = 0 //for debug
            
            while loc < dataLength {
                
                let rmdr = dataLength - loc
                if rmdr <= len {
                    len = rmdr
                }
                
                let range = NSMakeRange(loc, len)
                var newBytes = [UInt8](repeating: 0, count: len)
                (data as NSData).getBytes(&newBytes, range: range)
                let newData = Data(bytes: UnsafePointer<UInt8>(newBytes), count: len)
                // print("\(self.classForCoder.description()) writeRawData : packet_\(idx) : \(newData.hexRepresentationWithSpaces(true))")
                self.activePeripheral.writeValue(newData, for: destination!, type: writeType) // may be an issue here (no self.destination??)
                
                loc += len
                idx += 1
            }
        }
        
    }
    
    
    
    
    
    // MARK: CBCentralManagerDelegate
    
    /**
     Called when centralmanagers state changes (BLE is turned on/off from the phone).  Calls notification as response
     
     - parameter central: - the BLE central manager
     */
    open func centralManagerDidUpdateState(_ central: CBCentralManager) {
        var isOn = false
        if central.state == .poweredOff {
            print("BLE is turned off")
        }
        else if central.state == .poweredOn {
            print("BLE turned on")
            startScan(onlyForScopes: true)
            print("scanning for Sherburne")
            isOn = true
        }
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: AVScopeBLEChangedNotification),object: self,userInfo: ["BLE":isOn])

        
    }
    
    // Bluetooth found peripheral, check to make sure it's a Sherburne
    /**
     Called when a peripheral is discovered. Check to see if found peripheral is a Sherburne.  If it is, stop looking for other peripherals, connect to Sherburne, and set self as the delegate for Sherburne.  
     // if intentional or DFU disconnect is true, autoconnect will not happen and peripheral will be added to array of availible scopes
     
     - parameter central:           -BLE Central Manager
     - parameter peripheral:        - Peripheral that was found
     - parameter advertisementData: - Dictionary of advertisement data
     - parameter RSSI:              - strength of BLE connection
     */
    open func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        print(" found peripheral UUID: \(peripheral.identifier.uuidString)")
        print(" found peripheral name is : \(peripheral.name) ")
        
        if intentionalDisconnect || DFUDisconnect {
            // if getAvailibleScopeDevices  was called or in firmware mode
            
            //notify UI with peripheral object
            NotificationCenter.default.post(name: Notification.Name(rawValue: AVScopeFoundNotification),object: self,userInfo: ["foundScope": peripheral])
            // add to array for completion method
            availiblePeripherals.append(peripheral)
            
        }
        else {
            // device had disconnected accidentally, and should reconnect to active peripheral, tries to match either name or active peripheral
            if let activeName = activeScopeDevice?.name {
                print("active name = \(activeName)")
                if peripheral.name == activeName {
                    connect(peripheral)
                }
            }
            else if let realActivePeripheral = activePeripheral {
                if peripheral == realActivePeripheral {
                    connect(peripheral)
                }
            }
         
            else {
                print("found different scope - do not try to connect")
                
            }
        }
        
    }
    
    /**
     Called when Peripheral is connected.  Stops looking for other devices, and turns intentionaldisconnect to false
     
     - parameter central:    central managaer
     - parameter peripheral: peripheral that was connected
     */
    open func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        
        // make sure there is an active periph
        if activePeripheral != nil {
        //cancel connection timeout
        connectTimer.invalidate()
        
        print("Scope Connected")
        intentionalDisconnect = false
        isConnected = true
        
        //only call if in DFU mode 
        if DFUDisconnect {
        AVFirmwareUpdateService.sharedInstance.connectedDFUCompletionMethod()
        }
        //Look for services
        peripheral.discoverServices(nil)
    }
        else {
            print("active periph nil on did connect")
            // restart search
            connectionTimeout()
        }
    }
    
    open func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print("failed to establish connection")
        
        if ((error) != nil) {
            print("error connecting to peripheral \(peripheral.name)")
            print("error reason is : \(error?.localizedDescription)")
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: AVBLEErrorNotification),object: self,userInfo: ["reason":(error?.localizedDescription)!,"peripheral": peripheral.name!,"type": "connect"])
            
            
        }
        
        
    }
    
    
    /**
     Called when Peripheral is disconnected
     
     - parameter central:    central manager
     - parameter peripheral: peripheral that was disconnected
     - parameter error:
     */
    open func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        print("Disconnected ")
        
        //Update UI
        if  activeScopeDevice != nil {
             activeScopeDevice!.currentStatus = AVDeviceStatus.disconnected
        }
       
        NotificationCenter.default.post(name: Notification.Name(rawValue: AVStatusDidChangeNotification),object: self, userInfo: ["status":AVDeviceStatus.disconnected.rawValue])

        //change counter/flag stuff
        discoveredServicesCounter = 0
        isConnected = false
        
        // start looking for peripherals (look for anything if DFU caused disconnect
        if DFUDisconnect {
            startScan(onlyForScopes: false)
        }
        else {
        startScan(onlyForScopes: true)
        }
        // reset transfer timer if disconnect occured during transfer
        AVSnowProfileService.sharedInstance.transferTimer.invalidate()
        
        // error handling
        if ((error) != nil) {
            print("error disconnecting from peripheral \(peripheral.name)")
            print("error reason is : \(error?.localizedDescription)")
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: AVBLEErrorNotification),object: self,userInfo: ["reason":(error?.localizedDescription)!,"peripheral": peripheral.name!,"type": "disconnect"])
            
            
        }
    }
    
    
    
    // MARK: CBPeripheralDelegate
    
    /**
     called when peripheral is discovered
     
     - parameter peripheral: peripheral that was discovered
     - parameter error:
     */
    open func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        
        for service in peripheral.services! {
            let thisService = service
            print("service UUID: \(thisService.uuid.uuidString)")
            // if it is a service of Sherburne, look for characteristics
            if AVBLEInfo.sharedInstance.serviceUUID.values.contains(thisService.uuid)  {
                peripheral.discoverCharacteristics(nil, for: thisService)
                print("real UUID")
            }
            else {
                print("service found with invalid UUID (UUID = \(thisService.uuid) ")
            }
        }
    }
    
    /**
     Called when a write command is called.  Will throw error if a write is not successful
     
     - parameter peripheral:     peripheral to be written to
     - parameter characteristic: characteristic to be written to
     - parameter error:          error
     */
    open func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        
        let thisCharacteristic = characteristic.uuid
        
        print("I wrote succesfor characteristic \(thisCharacteristic)")
       
     
      /*
        switch characteristic {
           
        case nameCharacteristic!:
            activeScopeDevice!.confirmedNameChange()
        case firmwareDataCharacteristic! :
            testTimer.invalidate()
            print("total interval time = \(totalFirmwareInterval )")
        
        default:
            print("wrote to a characteristic that needs no confirmation")
        }
        */
        
            // If there is an error, notification method will be called
        if ((error) != nil) {
            print("error writing to characteristic =  \(thisCharacteristic)")
            print("error reason is : \(error?.localizedDescription)")
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: AVBLEErrorNotification),object: self,userInfo: ["reason":(error?.localizedDescription)!,"characteristic": "\(thisCharacteristic)","type": "write"])

            
        }
    }
    
    /**
     Called when characteristics are discovered for a service.  Looks through the characterisitics and subscribes to the ones that are indicated as "notify" in their characteristic properties. Also sets up characterisitic properties
     
     //additionally updates a counter each time a service is discovered, and calls the did connect method on AVscope when all services are discovered
     
     - parameter peripheral: - the peripheral that the service is a part of
     - parameter service:    - the service for which characteristics were found
     - parameter error:      <#error description#>
     */
    open func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        
        var thisCharacteristic = String()
        // loop through characteristics, check to see if notify is enabled.  If it is, make notification true
       

        for characteristic in service.characteristics! {
            print("Found Characteristics with UUID: \(characteristic.uuid.uuidString) ")
                       //check which service, assign current characteristic to specific service, change notify status
            let thisService = findStringForUUID(service.uuid, dictionary: AVBLEInfo.sharedInstance.serviceUUID)
            switch thisService! {
                
            case "Device Status":
                thisCharacteristic = findStringForUUID(characteristic.uuid, dictionary: AVBLEInfo.sharedInstance.DeviceStatusCharUUID)!
                let notifyStatus = AVBLEInfo.sharedInstance.DeviceStatusCharNotify[thisCharacteristic]
                switch thisCharacteristic {
                case "Status Type":
                    statusTypeCharacteristic = characteristic
                    peripheral.setNotifyValue(notifyStatus!, for: statusTypeCharacteristic!)
                default:
                    print("unknown characteristic")
                }
                
                
            case "Battery":
                thisCharacteristic = findStringForUUID(characteristic.uuid, dictionary: AVBLEInfo.sharedInstance.BatteryCharUUID)!
                let notifyStatus = AVBLEInfo.sharedInstance.BatteryCharNotify[thisCharacteristic]
                switch thisCharacteristic {
                case "Battery Level":
                    batteryLevelCharacteristic = characteristic
                    peripheral.setNotifyValue(notifyStatus!, for: batteryLevelCharacteristic!)
                default:
                    print("unknown characteristic")
                }
                
                
                
                
            case "Device Info":
               
                thisCharacteristic = findStringForUUID(characteristic.uuid, dictionary: AVBLEInfo.sharedInstance.DeviceInfoCharUUID)!
                let notifyStatus = AVBLEInfo.sharedInstance.DeviceInfoCharNotify[thisCharacteristic]
                
                switch thisCharacteristic {
                case "Hardware Revision":
                    hardwareRevisionCharacteristic = characteristic
                    peripheral.setNotifyValue(notifyStatus!, for: hardwareRevisionCharacteristic!)
                case "Serial Number":
                    serialNumberCharacteristic = characteristic
                    peripheral.setNotifyValue(notifyStatus!, for: serialNumberCharacteristic!)
                case "Model Number":
                    modelNumberCharacteristic = characteristic
                    peripheral.setNotifyValue(notifyStatus!, for: modelNumberCharacteristic!)
                case "Manufacturer Name":
                    manufacturerNameCharacteristic = characteristic
                    peripheral.setNotifyValue(notifyStatus!, for: manufacturerNameCharacteristic!)
                case "Firmware Version":
                    firmwareVersionCharacteristic = characteristic
                    peripheral.setNotifyValue(notifyStatus!, for: firmwareVersionCharacteristic!)
                case "System ID":
                    SystemIDCharacteristic = characteristic
                    peripheral.setNotifyValue(notifyStatus!, for: SystemIDCharacteristic!)
                default:
                    print("unknown characteristic")
                }
                
                
            case "Probing Errors":
                thisCharacteristic = findStringForUUID(characteristic.uuid, dictionary: AVBLEInfo.sharedInstance.ProbingErrorsCharUUID)!
                let notifyStatus = AVBLEInfo.sharedInstance.ProbingErrorsCharNotify[thisCharacteristic]
                
                switch thisCharacteristic {
                case "Error Type":
                    errorTypeCharacteristic = characteristic
                    peripheral.setNotifyValue(notifyStatus!, for: errorTypeCharacteristic!)
                default:
                    print("unknown characteristic")
                }
                
                
            case "Snow Profile":
                thisCharacteristic = findStringForUUID(characteristic.uuid, dictionary: AVBLEInfo.sharedInstance.SnowProfileCharUUID)!
                let notifyStatus = AVBLEInfo.sharedInstance.SnowProfileCharNotify[thisCharacteristic]
                
                switch thisCharacteristic {
                case "Profiles":
                    profileCharacteristic = characteristic
                    peripheral.setNotifyValue(notifyStatus!, for: profileCharacteristic!)
                case "Profiles ID":
                    profileIDCharacteristc = characteristic
                    peripheral.setNotifyValue(notifyStatus!, for: profileIDCharacteristc!)
                case "Profiles to transfer":
                    profilesToTransferCharacteristic = characteristic
                    peripheral.setNotifyValue(notifyStatus!, for: profilesToTransferCharacteristic!)
                case "Profiles to delete":
                    profilesToDeleteCharacteristic  = characteristic
                    peripheral.setNotifyValue(notifyStatus!, for: profilesToDeleteCharacteristic!)
                case "Location":
                    locationCharacteristic = characteristic
                    peripheral.setNotifyValue(notifyStatus!, for: locationCharacteristic!)
                case "Raw Data":
                    rawDataCharacteristic = characteristic
                    peripheral.setNotifyValue(notifyStatus!, for: rawDataCharacteristic!)
                case "Profile Length":
                    profileLengthCharacteristic = characteristic
                    peripheral.setNotifyValue(notifyStatus!, for: profileLengthCharacteristic!)
                    
                default:
                    print("unknown characteristic")
                }
                
            case "Slope":
                thisCharacteristic = findStringForUUID(characteristic.uuid, dictionary: AVBLEInfo.sharedInstance.SlopeCharUUID)!
                let notifyStatus = AVBLEInfo.sharedInstance.SlopeCharNotify[thisCharacteristic]
                
                switch thisCharacteristic {
                case "Slope Angle":
                    slopeAngleCharacteristic = characteristic
                    peripheral.setNotifyValue(notifyStatus!, for: slopeAngleCharacteristic!)
                default:
                    print("unknown characteristic")
                }
                
            case "Custom Info": // GET RID OF THIS!?!?!
                thisCharacteristic = findStringForUUID(characteristic.uuid, dictionary: AVBLEInfo.sharedInstance.CustomCharUUID)!
                let notifyStatus = AVBLEInfo.sharedInstance.CustomCharNotify[thisCharacteristic]
                
                switch thisCharacteristic {
                case "Name":
                    nameCharacteristic = characteristic
                    peripheral.setNotifyValue(notifyStatus!, for: nameCharacteristic!)
                default:
                    print("unknown characteristic")
                }
            case "Firmware":
                thisCharacteristic = findStringForUUID(characteristic.uuid, dictionary: AVBLEInfo.sharedInstance.FirmwareCharUUID)!
                let notifyStatus = AVBLEInfo.sharedInstance.FirmwareCharNotify[thisCharacteristic]
                
                switch thisCharacteristic {
                case "Data":
                    firmwareDataCharacteristic = characteristic
                    peripheral.setNotifyValue(notifyStatus!, for: firmwareDataCharacteristic!)
                    
                case "Command":
                    firmwareCommandCharacteristic = characteristic
                    peripheral.setNotifyValue(notifyStatus!, for: firmwareCommandCharacteristic!)
                default:
                    print("unknown characterisitc ")
                }
                
            default:
                print("unkown characteristic")
                discoveredServicesCounter -= 1
                print("serviceCount =\(discoveredServicesCounter)")

            }
            
          
        }
        // Counter for checking if all services and
        discoveredServicesCounter += 1
        print("serviceCount =\(discoveredServicesCounter)")
        if discoveredServicesCounter == 7 { // change to 7 with firmware service
            print("all characteristics discovered")
            AVScope.sharedInstance.didConnectWithCharacteristics()
            
        }
        if ((error) != nil) {
            print("error discovering characteristic =  \(thisCharacteristic)")
            print("error reason is : \(error?.localizedDescription)")
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: AVBLEErrorNotification),object: self,userInfo: ["reason":(error?.localizedDescription)!,"characteristic": thisCharacteristic,"type": "discoverCharacteristic"])
            
            
        }
        
    }
    
    
    
    var rawDataCounter = 0
    /**
     Called when a characteristic value is updated that was either read by a service or was set to notify.  Look through availible services and send updated characteristic value to whichever service the characteristic belongs to.
     - parameter peripheral:     - the peripheral that the characteristic belongs to
     - parameter characteristic:  - the characteristic that has been updated
     - parameter error:          <#error description#>
     */
    open func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        
        
        let rawData = characteristic.value
        //let rawDataLength = rawData!.length
        let currentService = characteristic.service
        
        
        let thisService = findStringForUUID(currentService.uuid, dictionary: AVBLEInfo.sharedInstance.serviceUUID)
        //print("new data = \(rawData) for service = \(thisService)")
        var thisCharacteristic = String()
        
        switch thisService! {
            
            
        case "Device Status":
            thisCharacteristic = findStringForUUID(characteristic.uuid, dictionary: AVBLEInfo.sharedInstance.DeviceStatusCharUUID)!
            switch thisCharacteristic {
            case "Status Type":
                let dataInt = rawData?.convertToInt()[0]
                if let rawStatus = AVDeviceStatus(status: dataInt!)?.rawValue {
                    if activeScopeDevice != nil {
                    activeScopeDevice!.currentStatus = AVDeviceStatus(status: dataInt!)
                    NotificationCenter.default.post(name: Notification.Name(rawValue: AVStatusDidChangeNotification),object: self, userInfo: ["status":rawStatus])
                    }
                }
                else {
                    print("invalid device status received = \(dataInt)")
                }
            default:
                print("unknown characteristic")
            }
            
            
             case "Battery":
             thisCharacteristic = findStringForUUID(characteristic.uuid, dictionary: AVBLEInfo.sharedInstance.BatteryCharUUID)!
             switch thisCharacteristic {
             case "Battery Level":
             AVBatteryService.sharedInstance.batteryUpdated(rawData!, error: error)
             default:
             print("unknown characteristic")
             }
             
 
            
            
        case "Device Info":
            thisCharacteristic = findStringForUUID(characteristic.uuid, dictionary: AVBLEInfo.sharedInstance.DeviceInfoCharUUID)!
            AVScope.sharedInstance.newDeviceInfo(rawData!, characteristic: thisCharacteristic)
            
            
            
        case "Probing Errors":
            thisCharacteristic = findStringForUUID(characteristic.uuid, dictionary: AVBLEInfo.sharedInstance.ProbingErrorsCharUUID)!
            switch thisCharacteristic {
            case "Error Type":
                AVSnowProfileService.sharedInstance.newProbingError(newData: rawData!)
               
                
            default:
                print("unknown characteristic")
            }
            
            
        case "Snow Profile":
            thisCharacteristic = findStringForUUID(characteristic.uuid, dictionary: AVBLEInfo.sharedInstance.SnowProfileCharUUID)!
            switch thisCharacteristic {
            case "Profiles":
                AVSnowProfileService.sharedInstance.enableRawData(false)
                AVSnowProfileService.sharedInstance.newProfiles(rawData!, error: error)
            case "Raw Data":
                rawDataCounter += rawData!.count
                //\\\print("rawDataCounter = \(rawDataCounter)")
                AVSnowProfileService.sharedInstance.enableRawData(true)
                AVSnowProfileService.sharedInstance.newProfiles(rawData!, error: error)
            case "Profiles ID":
                AVSnowProfileService.sharedInstance.newProfileIds(rawData!, error: error)
            case "Profile Error":
                do {
                    try AVSnowProfileService.sharedInstance.newProfileError(rawData!)
                }
                catch SnowProfileError.notFound{
                    print("profiles don't match")
                }
                catch {
                    print("other issue")
                }
            case "Profile Length":
                AVSnowProfileService.sharedInstance.updateExpectedTestLength(rawData!)
            default:
                print("unknown characteristic")
            }
            
             case "Slope":
             thisCharacteristic = findStringForUUID(characteristic.uuid, dictionary: AVBLEInfo.sharedInstance.SlopeCharUUID)!
             switch thisCharacteristic {
             case "Slope Angle":
             AVSlopeService.sharedInstance.slopeUpdated(rawData!,error: error)
             default:
             print("unknown characteristic")
             }
            
            case "Custom Info":
            thisCharacteristic = findStringForUUID(characteristic.uuid, dictionary: AVBLEInfo.sharedInstance.CustomCharUUID)!
            switch thisCharacteristic {
                case "Name":
                    // parse BLE and set = to name
                activeScopeDevice?.updateNameFromBLE(rawData!)
            default:
                print("unknown characteristic")
            }
            case "Firmware":
            thisCharacteristic = findStringForUUID(characteristic.uuid, dictionary: AVBLEInfo.sharedInstance.FirmwareCharUUID)!
            switch thisCharacteristic {
                case "Command":
                
                AVFirmwareUpdateService.sharedInstance.newPicFirmwareData(data: rawData!)
            default:
                print("received data from unknown characteristic")
            }
            
            
        default:
            print("unkown characteristic")
        }
        
        
        // ADD ERROR HANDLING (Error will be called if read was attempted but no value was returned
        
        if ((error) != nil) {
                print("error reading/notifying service =  \(thisService) ,characteristic = \(thisCharacteristic) ")
                print("error reason is : \(error?.localizedDescription)")
            
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: AVBLEErrorNotification),object: self,userInfo: ["reason":(error?.localizedDescription)!,"characteristic":thisCharacteristic,"type": "read"])
        }
        
    }
    
    
    
    
    
    /**
     Finds key for value in dictionary of Strings and CBUUIDs
     
     - parameter value:      UUID you are looking for
     - parameter dictionary: dictionary of [String: CBUUID]
     
     - returns: string that is key matching the UUID value
     */
    func findStringForUUID(_ value: CBUUID, dictionary: [String: CBUUID]) ->String? {
        for (key, uuid) in dictionary{
            if (value == uuid) {
                return key
            }
        }
        return nil
    }
    
    
    
    
}

//MARK: - Extensions
extension Data {
    //these data extensions are generally used for converting raw data from BLE into different object types
    
   
    
    
    /**
     breaks up a data object into an array of data objects with a maximum length 
     
    - parameter length: maximum length of each data object
    
     - returns: Array of data objects with maximum length.  First object has first bytes
 */
    func breakIntoPackets(ofLength: Int) -> [Data] {
        let totalDataLength = self.count
        // below length limit.  return single object array
        if totalDataLength < ofLength {
            return [self]
        }
            // above length limit
        else {
            var loc = 0 // current location in data
            var splitData = [Data]()
            
            while loc < totalDataLength {
                var dataRange: Range<Int> = loc..<(loc + ofLength)
                // check to make sure range doesn't go over max of data
                if loc + ofLength > totalDataLength {
                    dataRange = loc..<totalDataLength
                }
                // split data and add to array
                let dataSection = self.subdata(in: dataRange)
                splitData.append(dataSection)
                loc += ofLength
            }
            return splitData
        }
    }
    
    
    /**
     Converts data from chars in NSDATA to a String
     
     - parameter startIndex: starting index in data structure
     - parameter range:      length of data to convert
     
     - returns: String
     */
    
    func convertFromChars(_ startIndex: Int, range: Int)->String {
        print("bytes (as int8) to convert from char = \( convertFromInt8(startIndex,range: range))")

        
        //convert data to string & replace characters we can't display
        var data = [UInt8](repeating: 0, count: range)
        
        (self as NSData).getBytes(&data, range: NSRange(location: startIndex, length: range))
        
        
        for index in 0..<range {
            if (data[index] <= 0x1f) || (data[index] >= 0x80) { //null characters
                if (data[index] != 0x9)       //0x9 == TAB
                    && (data[index] != 0xa)   //0xA == NL
                    && (data[index] != 0xd) { //0xD == CR
                    data[index] = 0xA9
                }
                
            }
        }
        
        let newString = NSString(bytes: &data, length: range, encoding: String.Encoding.utf8.rawValue)
        
        return newString! as String
    }
    /**
     Converts data from Signed8 bit integers in NSDATA to an array of Ints
     
     - parameter startIndex: starting index in data structure
     - parameter range:      length of data to convert
     
     - returns: array of Ints
     */
    func convertFromSignedInt8(_ startIndex: Int, range: Int)->[Int] {
        
        var tempInts = [Int]()
        for idx in startIndex..<(startIndex + range) {
            var buff: Int8 = 0
            (self as NSData).getBytes(&buff, range: NSRange(location: idx, length: 1))
            tempInts.append(Int(buff))
        }
        // print("uint8s = \(tempInt)")
        return tempInts
    }
    
    
    /**
     Converts data from 8 bit integers in NSDATA to an array of Ints
     
     - parameter startIndex: starting index in data structure
     - parameter range:      length of data to convert
     
     - returns: array of Ints
     */
    func convertFromInt8(_ startIndex: Int, range: Int)->[Int] {
            var tempInts = [Int]()
        for idx in startIndex..<(startIndex + range) {
            var buff: UInt8 = 0
            (self as NSData).getBytes(&buff, range: NSRange(location: idx, length: 1))
            tempInts.append(Int(buff))
        }
        // print("uint8s = \(tempInt)")
        return tempInts
    }
    
    /**
     Converts data from 16 bit integers in NSDATA to an array of Ints
     
     - parameter startIndex: starting index in data structure
     - parameter range:      length of data to convert
     
     - returns: array of Ints
     */
    func convertFromInt16(_ startIndex: Int, range: Int)->[Int] {
       // print("bytes (as int8) to convert from int16 = \( convertFromInt8(startIndex,range: range))")

        var startingIndicies = [startIndex]
        for i in 1..<(range/2){
            startingIndicies.append(startIndex + i*2)
        }
        var tempInts = [Int]()
        for idx in startingIndicies {
            var buff: UInt16 = 0
            (self as NSData).getBytes(&buff, range: NSRange(location: idx, length: 2))
            tempInts.append(Int(buff))
        }
        //print("uint16s = \(tempInts)")
        
        return tempInts
    }
    /**
     Converts data from 32 bit floats in NSDATA to an array of Floats
     
     - parameter startIndex: starting index in data structure
     - parameter range:      length of data to convert
     
     - returns: array of Floats
     */
    func convertFromFloat(_ startIndex: Int, range: Int)->[Float] {
    print("bytes (as int8) to convert from float = \( convertFromInt8(startIndex,range: range))")
        var startingIndicies = [startIndex]
        for i in 1..<(range/4) {
            startingIndicies.append(startIndex + i*4)
        }
        var tempInts = [Float]()
        for idx in startingIndicies {
            var buff: Float32 = 0
            (self as NSData).getBytes(&buff, range: NSRange(location: idx, length: 4))
            tempInts.append(Float(buff))
        }
        //print("uint32s = \(tempInts)")
        
        return tempInts
    }
    /**
     Converts data from 32 bit integers in NSDATA to an array of Ints
     
     - parameter startIndex: starting index in data structure
     - parameter range:      length of data to convert
     
     - returns: array of Ints
     */
    func convertFromInt32(_ startIndex: Int, range: Int)->[Int] {
        
       print("bytes (as int8) to convert from int32 = \( convertFromInt8(startIndex,range: range))")
        
        var startingIndicies = [startIndex]
        for i in 1..<(range/4) {
            startingIndicies.append(startIndex + i*4)
        }
        var tempInts = [Int]()
        for idx in startingIndicies {
            var buff: UInt32 = 0
            (self as NSData).getBytes(&buff, range: NSRange(location: idx, length: 4))
            tempInts.append(Int(buff))
        }
        //print("uint32s = \(tempInts)")
        
        return tempInts
    }
    
    

    /**
     Converts NSData to array of Ints
     
     - returns: array of Ints
     */
    func convertToInt()->[Int] {
        let dataLength:Int = self.count
        var tempHard = [Int]()
        for idx in 0..<dataLength {
            var depthPt: Int = 0
            (self as NSData).getBytes(&depthPt, range: NSRange(location: idx, length: 1))
            tempHard.append(depthPt)
        }
        return tempHard
    }
    
    /**
     Converts NSData to String
     
     - returns: String
     */
    func stringRepresentation()->String {
        
        //Write new received data to the console text view
        
        //convert data to string & replace characters we can't display
        let dataLength:Int = self.count
        var data = [UInt8](repeating: 0, count: dataLength)
        
        (self as NSData).getBytes(&data, length: dataLength)
        
        for index in 0..<dataLength {
            if (data[index] <= 0x1f) || (data[index] >= 0x80) { //null characters
                if (data[index] != 0x9)       //0x9 == TAB
                    && (data[index] != 0xa)   //0xA == NL
                    && (data[index] != 0xd) { //0xD == CR
                    data[index] = 0xA9
                }
                
            }
        }
        
        let newString = NSString(bytes: &data, length: dataLength, encoding: String.Encoding.utf8.rawValue)
        
        return newString! as String
        
    }
    
    /**
     Converts NSData to SystemID
     
     - returns: UInts representing manufacturer ID and Orginization ID
     */
    func convertToSystemID()->(manufacturerID: UInt, OrginizationID: UInt) {
        var mnfID: UInt = 0
        (self as NSData).getBytes(&mnfID, range: NSRange(location: 0, length: 5))  //may need to change these
        print("mnfID  = \(mnfID)")
        var orgID: UInt = 0
        (self as NSData).getBytes(&orgID, range: NSRange(location: 5, length: 3))   //may need to change these
        print("orgID = \(orgID)")
        return (mnfID , orgID)
    }
    
    func hexEncodedString() -> String {
        return map { String(format: "%02hhx", $0) }.joined()
    }
  
    
}
/*
extension AVBLEManager {
    
    func writeString(_ string:NSString, forCharacteristic: String){
        
        //Send string to peripheral
        //hardnesses.removeAll(keepCapacity: false)
        let data = Data(bytes: string.utf8String!, count: string.length)
        print("writing \(string)")
        self.writeRawData(data, characteristic: forCharacteristic)
        
    }
} */

