//
//  scope-framework.h
//  scope-framework
//
//  Created by Olivier Brand on 2/25/16.
//  Copyright © 2016 Olivier Brand. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for scope-framework.
FOUNDATION_EXPORT double scope_frameworkVersionNumber;

//! Project version string for scope-framework.
FOUNDATION_EXPORT const unsigned char scope_frameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <scope_framework/PublicHeader.h>


