//
//  AVSlopeService.swift
//  scope-framework
//
//  Created by Olivier Brand on 2/25/16.
//  Copyright © 2016 Olivier Brand. All rights reserved.
//

import UIKit



/// The slope service defines the necessary functions to request slope
/// from a Scope device

open class AVSlopeService: NSObject {
  
    
  /** Singleton
   */
  open static let sharedInstance = AVSlopeService()
  

    /**
     Called when slope data is received from BLE.  Converts to integer and checks to see if it is a valid slope
     
     - parameter newData: slope data from BLE
     */
    open func slopeUpdated(_ newData: Data, error: Error?) {
        print("slope updated")
        let slopeAngle = newData.convertToInt()[0]
        if slopeAngle <= 90 && slopeAngle >= 0 {
            NotificationCenter.default.post(name: Notification.Name(rawValue: AVSlopeAngleNotification),object: self,userInfo: ["slope angle":slopeAngle])}
        else {
            print("invalid slope received = \(slopeAngle)")
        }
    }
    
    
    //PROBABLY DON'T NEED THE START/STOP update methods? Should always be sending slope when connected 
    
  /**
   Starts slope updates
   */
  open func startSlopeUpdates() {

  }
  
  /**
   Stops slope updates
   */
  open func stopSlopeUpdates() {
  }
  
}
