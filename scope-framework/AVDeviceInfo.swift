//
//  AVDeviceInfo.swift
//  scope-framework
//
//  Created by Olivier Brand on 3/2/16.
//  Copyright © 2016 Olivier Brand. All rights reserved.
//

import UIKit

/**
 Defined in the bluetooth characteristics
 The SYSTEM ID characteristic consists of a structure with two fields. The first field are the LSOs and the second field contains the MSOs.
 This is a 64-bit structure which consists of a 40-bit manufacturer-defined identifier concatenated with a 24 bit unique Organizationally
 Unique Identifier (OUI). The OUI is issued by the IEEE Registration Authority (http://standards.ieee.org/regauth/index.html) and is required
 to be used in accordance with IEEE Standard 802-2001.6 while the least significant 40 bits are manufacturer defined.
 */
open class AVSystemId {
  /** 40-bit manufacturer-defined identifier
  */
  open var manufacturerId : UInt
  /** 24 bit unique Organizationally Unique Identifier (OUI) issued by the IEEE Registration Authority
   */
  open var organizationallyUniqueId : UInt
  
  /**
   Constructs a AVSystemId per bluetooth specification
   
   - parameter manufacturerId:           40-bit manufacturer-defined identifier
   - parameter organizationallyUniqueId: 24 bit unique Organizationally Unique Identifier (OUI)
   
   - returns: Initialized AVSystemId
   */
  public init(manufacturerId:UInt, organizationallyUniqueId: UInt) {
    self.manufacturerId = manufacturerId
    self.organizationallyUniqueId = organizationallyUniqueId
  }
}

/**
 *  Defined in the bluetooth device_information service
 *  The Device Information Service exposes manufacturer and/or vendor information about a Scope device.
 */
open class AVDeviceInfo: NSObject {
    
    
  /** This characteristic represents the hardware revision for the hardware within the device.
   */
  open var hardwareRevision: String
  /** This characteristic represents the serial number for a particular instance of the device.
   */
  open var serialNumber : String
  /** This characteristic represents the model number that is assigned by the device vendor.
   */
  open var modelNumber : String
  /** This characteristic represents the name of the manufacturer of the device.
   */
  open var manufacturerName : String
  /** This characteristic represents the firmware revision for the firmware within the device.
   */
  open var firmwareRevision : String
  /** Defines the manufacturer-defined identifier and the unique Organizationally Unique Identifier (OUI)
   */
  open var systemID : AVSystemId
  
    
  /**
   Initializes the device info object per bluetooth specification
   
   - parameter hardwareRevision: serial number for a particular instance of the device
   - parameter serialNumber:     serial number for a particular instance of the device
   - parameter modelNumber:      the model number that is assigned by the device vendor
   - parameter manufacturerName: the name of the manufacturer of the device
   - parameter firmwareRevision: the firmware revision for the firmware within the device
   - parameter systemID:         the manufacturer-defined identifier and the unique Organizationally Unique Identifier (OUI)
   
   - returns: Initialized AVDeviceInfo
   */
  public init(hardwareRevision: String, serialNumber : String, modelNumber: String, manufacturerName : String, firmwareRevision : String, systemID : AVSystemId) {
    self.hardwareRevision = hardwareRevision
    self.serialNumber = serialNumber
    self.modelNumber = modelNumber
    self.manufacturerName = manufacturerName
    self.firmwareRevision = firmwareRevision
    self.systemID = systemID
  }
  
}
