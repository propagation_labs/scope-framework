jazzy \
  --clean \
  --author Avatech\
  --author_url http://avatech.com \
  --github_url https://github.com/avatech-inc/scope-framework \
  --module-version 0.1 \
  --xcodebuild-arguments -scheme,scope-framework \
  --module scopeFramework \
  --output docs/swift_output \
  --theme apple

